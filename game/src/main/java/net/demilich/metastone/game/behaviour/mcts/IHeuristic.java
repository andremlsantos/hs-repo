package net.demilich.metastone.game.behaviour.mcts;

import net.demilich.metastone.game.GameContext;
import net.demilich.metastone.game.actions.GameAction;

//para criarmos heuristica generica, implemetamos esta interface
public interface IHeuristic {

	double evaluate(GameContext state, GameAction action );
}
