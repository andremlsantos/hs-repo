package net.demilich.metastone.game.behaviour.mcts;

import java.util.concurrent.Callable;
import net.demilich.metastone.game.GameContext;
import net.demilich.metastone.game.behaviour.Behaviour;
import net.demilich.metastone.game.decks.Deck;
import net.demilich.metastone.utils.Tuple;

//usado no calculo do fistness
//criamos pool de jogos
//comecamos todos os jogos do fitness ao mesmo tempo:
//a vs a
//a vs b
//a vs c
//a vs d
//a vs e
//a vs f
public class PlayDeckTask implements Callable<Void> {
	private int individualGames;
	private Chromosome chromosome;
	private String deck2Name, deck1Name, deck1FileName;
	private GeneticTrade trade;
	private Behaviour opponent;

	public Behaviour getOpponent() {
		return opponent;
	}

	public void setOpponent(Behaviour opponent) {
		this.opponent = opponent;
	}

	private Deck deck1, deck2;

	public PlayDeckTask(int games, Chromosome chromesone) {
		this.individualGames = games;
		this.chromosome = chromesone;
	}

	@Override
	public Void call() throws Exception {
		int individualVictories = 0;
		
		for (int i = 0; i < individualGames; i++) {
			GameContext game = null;
			
			synchronized (this.trade) {
				game = this.trade.createGame(this.chromosome, this.opponent, this.deck1, this.deck2);
			}	
			//System.out.println(deck1.getName() + " vs " + deck2.getName() + " starts ");
			game.play();
			
			//System.out.println(deck1.getName() + " vs " + deck2.getName() + " ends ");

			if (game.getWinningPlayerId() == 0)
				individualVictories++;
			game.dispose();
		}

		synchronized (chromosome) {
			Tuple<String, Integer> stat = new Tuple<String, Integer>(this.deck2Name, individualVictories);
			chromosome.getStats().insertValue(stat);
			chromosome.getStats().setDeck1(this.deck1Name);
			chromosome.getStats().setDeck1FileName(this.deck1FileName);
			chromosome.incrementFitness(individualVictories);
		}
		return null;
	}

	public Deck getDeck1() {
		return deck1;
	}

	public String getDeck1FileName() {
		return deck1FileName;
	}

	public String getDeck1Name() {
		return deck1Name;
	}
	public Deck getDeck2() {
		return deck2;
	}

	public String getDeck2Name() {
		return deck2Name;
	}

	public GeneticTrade getTrade() {
		return trade;
	}

	public void setDeck1(Deck deck1) {
		this.deck1 = deck1;
		this.deck1Name = deck1.getName();
		this.deck1FileName = deck1.getFilename();
	}

	public void setDeck1FileName(String deck1FileName) {
		this.deck1FileName = deck1FileName;
	}

	public void setDeck1Name(String deck1Name) {
		this.deck1Name = deck1Name;
	}

	public void setDeck2(Deck deck2) {
		this.deck2 = deck2;
		this.deck2Name = deck2.getName();
	}

	public void setDeck2Name(String deck2Name) {
		this.deck2Name = deck2Name;
	}

	public void setTrade(GeneticTrade trade) {
		this.trade = trade;
	}
}
