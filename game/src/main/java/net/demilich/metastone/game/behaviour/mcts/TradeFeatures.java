package net.demilich.metastone.game.behaviour.mcts;

/*
 * forma de como calculo o trade
 * HEURISTIC 1
 * */
public enum TradeFeatures {
	//minions
	FriendlyHP(1),
	FriendlyATK(1),
	AdversaryHP(1),
	AdversaryATK(1),
	
	//hero
	FriendlyHeroHp(1),
	FriendlyHeroATK(1),
	AdversaryHeroHp(1),
	AdversaryHeroATK(1);
	
	double weight;
	
	TradeFeatures() { }
	
	private TradeFeatures(double weight) {
		this.weight = weight;
	}
	
	public double getWeight() {
		return weight;
	}
	
	void setWeight(double weight) {
		this.weight = weight;
	}
}
