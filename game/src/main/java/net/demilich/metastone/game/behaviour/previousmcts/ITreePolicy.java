package net.demilich.metastone.game.behaviour.previousmcts;

interface ITreePolicy {

	Node select(Node parent);
}
