package net.demilich.metastone.game.behaviour.mcts;

//apos o budget terminar, temos 4 metodos para selecionarmos a acao da arvore
public class SelectAction {

	//criterio selecionado
	WinningSelectionCriteria criteria;

	public SelectAction(WinningSelectionCriteria criteria) {
		this.criteria = criteria;
	}

	public Node selectAction(Node root) {
		Node result = null;

		switch (criteria) {
		case MaxChild:
			result = maxChild(root);
			break;
		case RobustChild:
			result = robustChild(root);
			break;
		case MaxRobustChild:
			result = maxRobustChild(root);
			break;
		case SecureChild:
			result = secureChild(root);
			break;
		default:
			break;
		}

		//uma vez que vamos retornar a acao, passamos estas flags a true
		MctsBehaviour.isReturningAction = true;
		MctsBehaviour.returningActionNode = result;

		return result;
	}

	/*
	 * Select the root child with the highest reward
	 */
	private Node maxChild(Node root) {

		int reward = Integer.MIN_VALUE;
		Node maxChild = null;

		for (Node child : root.getChildren()) {

			if (child.getVictories() >= reward && child.getAction() != null) {
				reward = child.getVictories();
				maxChild = child;
			}
		}

		return maxChild;
	}

	/*
	 * Select the most visited root child
	 */
	private Node robustChild(Node root) {

		int visits = Integer.MIN_VALUE;
		Node robustChild = null;

		for (Node child : root.getChildren()) {

			if (child.getVisits() >= visits && child.getAction() != null) {
				visits = child.getVisits();
				robustChild = child;
			}
		}

		return robustChild;
	}

	/*
	 * Select the root child with both the highest visit count and the highest
	 * reward
	 */
	private Node maxRobustChild(Node root) {

		int visits = Integer.MIN_VALUE, reward = Integer.MIN_VALUE;
		Node maxRobustChild = null;

		for (Node child : root.getChildren()) {

			if (child.getVictories() >= reward && child.getVisits() >= visits && child.getAction() != null) {
				reward = child.getVictories();
				visits = child.getVisits();
				maxRobustChild = child;
			}
		}

		return maxRobustChild;
	}

	/*
	 * Select the child which maximises a lower confidence bound
	 */
	private Node secureChild(Node root) {

		double maxValue = Double.MIN_VALUE;
		Node maxLCB = null;

		for (Node child : root.getChildren()) {

			double currentValue = LCB(child, root, TreePolicy.cp);

			if (currentValue > maxValue && child.getAction() != null) {
				maxValue = currentValue;
				maxLCB = child;
			}
		}

		return maxLCB;
	}

	private double LCB(Node child, Node parent, double cp) {
		return child.getVictories() / child.getVisits()
				- cp * Math.sqrt((2 * Math.log(parent.getVisits())) / child.getVisits());
	}
}
