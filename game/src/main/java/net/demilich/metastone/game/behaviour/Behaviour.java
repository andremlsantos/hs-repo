package net.demilich.metastone.game.behaviour;

import net.demilich.metastone.game.GameContext;
import net.demilich.metastone.game.Player;
import net.demilich.metastone.game.actions.GameAction;
import net.demilich.metastone.game.behaviour.mcts.IHeuristic;

public abstract class Behaviour implements IBehaviour {

	public IBehaviour clone() {
		try {
			return (IBehaviour) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void onGameOver(GameContext context, int playerId, int winningPlayerId) {
	}
	
	protected double simulateAction(GameContext simulation, Player player, GameAction action, IHeuristic heuristic) {
		simulation.getLogic().performGameAction(player.getId(), action);
		
		if(heuristic!=null) {
			double score = heuristic.evaluate(simulation, action);
			simulation.dispose();
			return score;
		} 
		return 0;
	}
}
