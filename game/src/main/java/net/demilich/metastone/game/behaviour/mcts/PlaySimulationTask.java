package net.demilich.metastone.game.behaviour.mcts;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.sun.org.apache.regexp.internal.recompile;

import net.demilich.metastone.game.GameContext;
import net.demilich.metastone.game.actions.ActionType;
import net.demilich.metastone.game.actions.GameAction;
import net.demilich.metastone.game.behaviour.Behaviour;

public class PlaySimulationTask implements Callable<Integer> {
	private GameContext game;
	private Behaviour player, opponent;
	private int playerId, victory;

	public PlaySimulationTask(GameContext context, Behaviour player, Behaviour opponent, int playerId) {
		this.game = context;
		this.player = (Behaviour) player.clone();
		this.opponent = (Behaviour) opponent.clone();
		this.playerId = playerId;
	}

	@Override
	public Integer call() throws Exception {
		return execute();
	}

	private int execute() {
		//System.out.println("start " + game.getSimulationIntex());
		while (!game.gameDecided()) {
			GameAction actionReturned = null;
			List<GameAction> validActions = game.getValidActions();

			if (game.getActivePlayerId() == playerId) {
				actionReturned = player.requestAction(game, game.getActivePlayer(), validActions);
				game.performAction(game.getActivePlayerId(), actionReturned);
			} else {
				actionReturned = opponent.requestAction(game, game.getActivePlayer(), validActions);
				game.performAction(game.getActivePlayerId(), actionReturned);
			}

			// end turn state
			if (actionReturned.getActionType() == ActionType.END_TURN)
				game.startTurn(game.getActivePlayerId());
		}

		this.victory = game.getWinningPlayerId() == playerId ? 1 : 0;
		//System.out.println("end " + game.getSimulationIntex());

		game.dispose();
		
		return this.victory;
	}
}
