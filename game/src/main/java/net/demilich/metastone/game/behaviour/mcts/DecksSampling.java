package net.demilich.metastone.game.behaviour.mcts;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import net.demilich.metastone.game.Player;
import net.demilich.metastone.game.cards.Card;
import net.demilich.metastone.game.cards.CardCollection;
import net.demilich.metastone.game.decks.Deck;
import net.demilich.metastone.game.entities.Entity;
import net.demilich.metastone.game.entities.heroes.HeroClass;
import net.demilich.metastone.game.entities.minions.Minion;
import net.demilich.metastone.gui.deckbuilder.DeckProxy;
import net.demilich.metastone.gui.mctsmode.DecksDB;
import net.demilich.metastone.gui.mctsmode.ReadDBCommand;
import net.demilich.metastone.utils.Tuple;

public class DecksSampling {

	private List<Deck> allDecks;			//todos os decks presentes no simulador
	private List<Deck> selecteDecks;		//apenas os decks selecionados no ecra configuracao de parametros mcts

	public DecksSampling() {
		readSelectedDecks();
	}

	public List<Deck> getAllDecks() {
		return allDecks;
	}

	public void setAllDecks(List<Deck> allDecks) {
		this.allDecks = allDecks;
	}

	public List<Deck> getSelecteDecks() {
		return selecteDecks;
	}

	//obter decks selecionados por classe de heroi
	public List<Deck> getSelectedHeroDecks(HeroClass hero) {
		List<Deck> result = new ArrayList<>();

		for (Deck deck : selecteDecks) {
			if (deck.getHeroClass().equals(hero))
				result.add(deck.clone());
		}

		return result;
	}

	public void setSelecteDecks(List<Deck> selecteDecks) {
		this.selecteDecks = selecteDecks;
	}

	//obter lista decks selecionados no ecra de configuracao de parametros mcts
	private void readSelectedDecks() {

		DeckProxy deckProxy = new DeckProxy();
		try {
			deckProxy.loadDecks();
			this.allDecks = deckProxy.getDecks();

			DecksDB.getInstance().setBuilderDecks(this.allDecks);

			ReadDBCommand command = new ReadDBCommand();
			command.execute();

			this.selecteDecks = DecksDB.getInstance().getJsonDecks();

		} catch (IOException | URISyntaxException e) {
			System.out.println("Error reading all decks from Sampling");
		}
	}

	//obter o deck mais provavel que o oponente pode ter
	//de acordo o seu historico de jogadas
	public Deck execute(Player opponent) {
		// we save the # occurences and respective deck
		List<Tuple<Integer, Deck>> result = new ArrayList<>();

		// decks selected on DECKSDATABASE
		List<Deck> samplingDecks = getSelectedHeroDecks(opponent.getHero().getHeroClass());

		for (int i = 0; i < samplingDecks.size(); i++) {

			Deck deck = samplingDecks.get(i);
			
			int size = deck.getCards().getCount();
			
			if(size!=30) {
				System.out.println("O deck size � de " + size);
				return null;
			}
			
			int occurrences = 0;

			// graveyard includes minions played but not dead
			//semelhante ao historico de jogadas
			for (Entity card : opponent.getGraveyard()) {
				occurrences += countOcurrences(deck, card);
			}

			//resultado, numero de ocorrencias, deck
			Tuple<Integer, Deck> pair = new Tuple<Integer, Deck>(occurrences, deck);
			result.add(pair);
		}

		// WE RETURN THE BEST MATCH DECK
		int bestNOccurrences = Integer.MIN_VALUE;
		Deck mostPropableDeck = null;

		for (Tuple<Integer, Deck> pair : result) {

			if (pair.getFirst() > bestNOccurrences) {
				mostPropableDeck = pair.getSecond();
				bestNOccurrences = pair.getFirst();
			} else if (pair.getFirst() == bestNOccurrences) {
				// random
				Random randomizer = new Random();
				int swap = randomizer.nextInt(2);

				if (swap == 1) {
					mostPropableDeck = pair.getSecond();
					bestNOccurrences = pair.getFirst();
				}
			}
		}
		//System.out.println("sai do execute DeckSampling");
		return mostPropableDeck;
	}

	//auxiliar, conta o numero de vezes que a carta card aparece em deck
	private int countOcurrences(Deck deck, Entity card) {
		int count = 0;

		CardCollection cardColection = deck.getCards();

		for (Card myCard : cardColection.toList()) {
			if (card.getName().equals(myCard.getName()))
				count++;
		}
		return count;
	}

	//apos fazermos a amostragem, removemos as cartas ja jogadas
	public void removePlayedCards(Player player, Deck sampleDeck) {
		List<String> gamingHistory = new ArrayList<>();

		// historic plays
		for (Entity cardPlayed : player.getGraveyard()) {
			gamingHistory.add(cardPlayed.getName());
		}

		//in METASTONE, the same card is present in booth minions list & graveyard
		//we need to remove duplicates!
		gamingHistory = removeDuplicates(gamingHistory);
		
		for(String cardPlayed : gamingHistory) {
			removeFirstCardOccurence(cardPlayed, sampleDeck);
		}
	}

	// AUX to remove cards
	private List<String> removeDuplicates(List<String> history) {
		List<String> noDuplicates = new ArrayList<>();

		for (String move : history) {
			if (!noDuplicates.contains(move)) {
				// contar
				int count = Math.min(countReferences(move, history), 4);	//bug?
				
				switch (count) {
				case 1:
				case 2:
				case 3:
					noDuplicates.add(move);
					break;
				case 4:
					noDuplicates.add(move);
					noDuplicates.add(move);
					break;
				default:
					break;
				}
			}
		}
		return noDuplicates;
	}

	//auxiliar que conta numero de occorencias de uma jogada no historico de jogadas
	private int countReferences(String move, List<String> history) {
		int count = 0;

		for (String play : history) {
			if (play.equals(move))
				count++;
		}

		return count;
	}
	
	//auxiliar devido � forma de como o historico de jogadas � implementado
	//uma carta que tenha sido jogada pode aparecer entre 1 a 3 vezes no historico
	//uma carta ou minion que ja morreu aparece 4 vezes no hsitorico
	//para removermos estas inconsistencias, desenvolvi este metodo auxiliar
	//foi assim que metastone foi feito, bem vindo!
	private void removeFirstCardOccurence(String name, Deck deck) {
		int nCards = deck.getCards().getCount(), i=0;
		boolean removed = false;
		
		while(!removed && i < nCards) {
			if(deck.getCards().get(i).getName().equals(name)) {
				deck.getCards().remove(i);
				removed = true;
			}
			i++;
		}		
	}
}
