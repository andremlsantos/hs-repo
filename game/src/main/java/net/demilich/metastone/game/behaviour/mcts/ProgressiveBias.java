package net.demilich.metastone.game.behaviour.mcts;

//integrar heuristica na tree policy
public class ProgressiveBias {

	//our heuristic
	private IHeuristic heuristic = null;
	
	public ProgressiveBias(IHeuristic heuristic) {
		this.heuristic = heuristic;
	}
	
	//we evaluate the node
	//optimization! if the node is already evaluated, 
	//than return such evaluation
	public double evaluate(Node node) {
		if(this.heuristic == null)
			return 0;
		
		if(!node.isEvaluated()) {
			double value = heuristic.evaluate(node.getState(), node.getAction());
			node.setEvaluated(true);
			node.setHeuristicValue(value);			
		}
		
		return  node.getHeuristicValue() / (node.getVisits() + 1);
	}
}
