package net.demilich.metastone.game.behaviour.mcts;

public class Chromosome implements Comparable<Chromosome>{

	//propriedades de um cromossoma
	private String label;
	private double[] values;
	private double fitness = 0;
	private double[] fitnessRatio = { -1, -1 };
	private int rank=-1;
	private ChromosomeStats stats = new ChromosomeStats();

	public Chromosome() {
		this.values = new double[Genetic.chromossomeSize];
		
		for(int i=0; i<Genetic.chromossomeSize; i++) {
			this.values[i] = -1;
		}
	}
	
	//para clonarmos um cromossoma
	public Chromosome clone() {
		Chromosome result = new Chromosome();
		result.values = this.values;
		result.label = this.label;
		result.fitness = this.fitness;
		result.stats = this.stats;

		return result;
	}
	
	//LIMPAR propriedades de um cromossoma
	public void clear() {
		this.fitness = 0;
		this.rank = -1;
		this.stats = new ChromosomeStats();		
	}

	//para ordenarmos a populacao de acordo o fitness de 3 cromossomas
	@Override
	public int compareTo(Chromosome o) {
		double[] init = {-1, -1};
		this.fitnessRatio = init;
		o.fitnessRatio = init;
		
		return (int) Math.round(o.getFitness() - this.getFitness());
	}

	public String getChromosomeLabel() {
		return label;
	}

	public double getFitness() {
		return fitness;
	}

	public double[] getFitnessRatio() {
		return fitnessRatio;
	}

	public int getRank() {
		return rank;
	}

	public ChromosomeStats getStats() {
		return stats;
	}

	public double[] getValues() {
		return values;
	}

	public void setChromosomeLabel(String chromosomeLabel) {
		this.label = chromosomeLabel;
	}

	public void setFitness(double fitness) {
		this.fitness = fitness;
	}

	//calculo do fitness ratio
	//usado eplo metodo selecao roleta
	public void setFitnessRatio(double fitnessRatio, int total, Chromosome chromosome) {
		if (chromosome == null) {
			this.fitnessRatio[0] = 0;
			this.fitnessRatio[1] = fitnessRatio;
			Genetic.lastRatio = this.fitnessRatio[1];
		} 
		else if (total > 0 && fitnessRatio > 0) {
			this.fitnessRatio[0] = Genetic.lastRatio;
			this.fitnessRatio[1] = fitnessRatio + this.fitnessRatio[0];
			Genetic.lastRatio = this.fitnessRatio[1];
		} 
		else if (total > 0 && fitnessRatio == 0) {
			this.fitnessRatio[0] = 0;
			this.fitnessRatio[1] = 0;
		}
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public void setStats(ChromosomeStats stats) {
		this.stats = stats;
	}

	public void setValues(double[] chromosome) {
		this.values = chromosome;
	}
	
	public void incrementFitness(int value) {
		this.fitness += value;
	}
}
