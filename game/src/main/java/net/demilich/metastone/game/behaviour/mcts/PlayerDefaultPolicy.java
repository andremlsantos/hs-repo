package net.demilich.metastone.game.behaviour.mcts;

import java.util.ArrayList;
import java.util.List;
import net.demilich.metastone.game.GameContext;
import net.demilich.metastone.game.Player;
import net.demilich.metastone.game.actions.GameAction;
import net.demilich.metastone.game.behaviour.Behaviour;
import net.demilich.metastone.game.cards.Card;

//ia usada pelo mcts na simulacao
//maximizamos o valor heuristica das k acoes 
public class PlayerDefaultPolicy extends Behaviour {

	private static final String name = "PlayerDefaultPolice";

	// heuristic
	private IHeuristic heuristic;
	private int kActions, baseActions, percentage;
	private boolean isDynamic;

	public PlayerDefaultPolicy(IHeuristic heuristc, boolean isDynamic, int minK, int percentage) {
		this.heuristic = heuristc;
		this.baseActions = minK;
		this.kActions = minK;
		this.isDynamic = isDynamic;
		this.percentage = percentage;
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public List<Card> mulligan(GameContext context, Player player, List<Card> cards) {
		return new ArrayList<>();
	}

	@Override
	public GameAction requestAction(GameContext context, Player player, List<GameAction> validActions) {
		this.kActions = DefaultPolicy.setupK(validActions, isDynamic, baseActions, percentage);

		double heuristicValue = Double.NEGATIVE_INFINITY;
		GameAction bestHeuristicAction = null;

		List<GameAction> randomActions = DefaultPolicy.tounementSelection(validActions, kActions);

		for (GameAction gameAction : randomActions) {

			// state clone
			double score = simulateAction(context.clone(), player, gameAction, heuristic);

			if (score >= heuristicValue) {
				heuristicValue = score;
				bestHeuristicAction = gameAction;
			}
		}
		return bestHeuristicAction;
	}
}
