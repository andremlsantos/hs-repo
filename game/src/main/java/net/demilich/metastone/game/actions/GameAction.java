package net.demilich.metastone.game.actions;

import net.demilich.metastone.game.GameContext;
import net.demilich.metastone.game.Player;
import net.demilich.metastone.game.entities.Entity;
import net.demilich.metastone.game.targeting.EntityReference;
import net.demilich.metastone.game.targeting.TargetSelection;

public abstract class GameAction implements Cloneable {

	private TargetSelection targetRequirement = TargetSelection.NONE;
	private ActionType actionType = ActionType.SYSTEM;
	private EntityReference source;
	private EntityReference targetKey;
	private String actionSuffix;

	public boolean canBeExecutedOn(GameContext gameContext, Player player, Entity entity) {
		return true;
	}

	@Override
	public GameAction clone() {
		GameAction action = null;
		try {
			action = (GameAction) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return action;
	}

	public abstract void execute(GameContext context, int playerId);

	public String getActionSuffix() {
		return actionSuffix;
	}

	public ActionType getActionType() {
		return actionType;
	}

	public abstract String getPromptText();

	public EntityReference getSource() {
		return source;
	}

	public EntityReference getTargetKey() {
		return targetKey;
	}

	public TargetSelection getTargetRequirement() {
		return targetRequirement;
	}

	public abstract boolean isSameActionGroup(GameAction anotherAction);

	public void setActionSuffix(String actionSuffix) {
		this.actionSuffix = actionSuffix;
	}

	protected void setActionType(ActionType actionType) {
		this.actionType = actionType;
	}

	public void setSource(EntityReference source) {
		this.source = source;
	}

	public void setTarget(Entity target) {
		this.targetKey = EntityReference.pointTo(target);
	}

	public void setTargetKey(EntityReference targetKey) {
		this.targetKey = targetKey;
	}

	public void setTargetRequirement(TargetSelection targetRequirement) {
		this.targetRequirement = targetRequirement;
	}

	// MCTS AUX METHODS
	public int getId(GameAction action) {
		if (action.actionType.equals(ActionType.SPELL) || action.actionType.equals(ActionType.EQUIP_WEAPON)
				|| action.actionType.equals(ActionType.SUMMON) || action.actionType.equals(ActionType.HERO_POWER)) {
			return ((PlayCardAction) action).getCardReference().getCardId();
		} 
		else if (action.actionType.equals(ActionType.PHYSICAL_ATTACK)) {
			return ((PhysicalAttackAction) action).getAttackerReference().getId();
		}
		else if (action.actionType.equals(ActionType.BATTLECRY)) {
			BattlecryAction battlecryAction = (BattlecryAction) action;
			return battlecryAction.getSource().getId();
		}
		else if (action.actionType.equals(ActionType.DISCOVER)) {
			
		}

		return -1;
	}

	public String getName(GameAction action) {
		if (action.actionType.equals(ActionType.SPELL) || action.actionType.equals(ActionType.EQUIP_WEAPON)
				|| action.actionType.equals(ActionType.SUMMON) || action.actionType.equals(ActionType.HERO_POWER)) {
			return ((PlayCardAction) action).getCardReference().getCardName();
		} 
		else if (action.actionType.equals(ActionType.PHYSICAL_ATTACK)) {
			return ((PhysicalAttackAction) action).getName();
		} 
		else if (action.actionType.equals(ActionType.BATTLECRY)) {
			BattlecryAction battlecryAction = (BattlecryAction) action;
			return battlecryAction.getSpell().getSpellClass().getSimpleName();
		} 
		else if (action.actionType.equals(ActionType.DISCOVER)) {
			return ((DiscoverAction)action).getSpell().getSpellClass().getSimpleName();
		}

		return "-";
	}

	public int getTarget(GameAction action) {
		if (action.actionType.equals(ActionType.SPELL) || action.actionType.equals(ActionType.EQUIP_WEAPON)
				|| action.actionType.equals(ActionType.SUMMON) || action.actionType.equals(ActionType.HERO_POWER)) {
			PlayCardAction playCardAction = ((PlayCardAction) action);
			return playCardAction.getTargetKey() == null ? -1 : playCardAction.getTargetKey().getId();
		} 
		else if (action.actionType.equals(ActionType.PHYSICAL_ATTACK)) {
			return ((PhysicalAttackAction) action).getTargetKey().getId();
		} 
		else if (action.actionType.equals(ActionType.BATTLECRY)) {
			BattlecryAction battlecryAction = (BattlecryAction) action;
			EntityReference target = battlecryAction.getSpell().hasPredefinedTarget() ? battlecryAction.getSpell().getTarget() : getTargetKey();			
			return target.getId();
		} 
		else if (action.actionType.equals(ActionType.DISCOVER)) {
			DiscoverAction battlecryAction = (DiscoverAction) action;
			EntityReference target = battlecryAction.getSpell().hasPredefinedTarget() ? battlecryAction.getSpell().getTarget() : getTargetKey();
			return target.getId();
		}

		return -1;
	}
}
