package net.demilich.metastone.game.behaviour.mcts;

import java.util.ArrayList;
import java.util.List;

import net.demilich.metastone.game.GameContext;
import net.demilich.metastone.game.Player;
import net.demilich.metastone.game.actions.ActionType;
import net.demilich.metastone.game.actions.EndTurnAction;
import net.demilich.metastone.game.actions.GameAction;
import net.demilich.metastone.game.decks.Deck;
import net.demilich.metastone.game.entities.heroes.Hero;
import net.demilich.metastone.gui.mctsmode.DecksDB;

public class Node {

	//state
	private GameContext state;
	
	//statistics
	private int nVisits = 0;			// N
	private int nVictories = 0;			// Q
	
	//actions
	private GameAction action = null;
	private List<GameAction> availableActions = new ArrayList<>();
	
	//nodes
	private Node parent = null;
	private ArrayList<Node> children = new ArrayList<>();
	
	//active player
	private Player player;
	
	//heuristic value
	private boolean isEvaluated = false;
	private double heuristicValue = -1;
	
	public Node(GameContext state) {
		this.state = state.clone();									//clone the actual state
		this.state.setMCTS(true);									//we declare its an mcts node
		this.availableActions = this.state.getValidActions();		//actions
		this.player = this.state.getActivePlayer();					//we save the actual player
	}
	
	public Node(GameContext state, List<GameAction> availableActions) {
		this.state = state.clone();									//clone the actual state
		this.state.setMCTS(true);									//we declare its an mcts node
		this.availableActions = availableActions;					//actions
		this.player = this.state.getActivePlayer();					//we save the actual player
	}
	
	public GameAction getAction() {
		return action;
	}

	public List<GameAction> getAvailableActions() {
		return availableActions;
	}

	public ArrayList<Node> getChildren() {
		return children;
	}

	public double getHeuristicValue() {
		return heuristicValue;
	}
	
	public Node getParent() {
		return parent;
	}

	public Player getPlayer() {
		return player;
	}

	public GameContext getState() {
		return state;
	}
	
	public int getVictories() {
		return nVictories;
	}

	public int getVisits() {
		return nVisits;
	}

	//back propagation
	public void increaseVictories(int reward) {
		this.nVictories += reward;
	}

	//back propagation
	public void increaseVisits(int reward) {
		this.nVisits += reward;
	}

	//optimizacao na tree policy
	//se no ja foi avaliado, nao precisamos de o avaliar outravez
	public boolean isEvaluated() {
		return isEvaluated;
	}

	//temos sempre a�ao terminar turno
	public boolean isFullyExpanded() {
		return availableActions.size() == 0 ? true : false;
	}

	//se houver vencedor
	public boolean isTerminal() {
		if(this.state==null)
			System.out.println("null");
		
		return this.state.gameDecided();
	}
	
	
	public void setAction(GameAction action) {			
		this.action = action;
	}

	public void setAvailableActions(List<GameAction> untriedActions) {
		this.availableActions = untriedActions;
	}
	
	
	public void setChildren(ArrayList<Node> children) {
		this.children = children;
	}
	
	public void setEvaluated(boolean isEvaluated) {
		this.isEvaluated = isEvaluated;
	}

	public void setHeuristicValue(double heuristicValue) {
		this.heuristicValue = heuristicValue;
	}

	public void setnVictories(int nVictories) {
		this.nVictories = nVictories;
	}

	public void setnVisits(int nVisits) {
		this.nVisits = nVisits;
	}

	public void setParent(Node parent) {
		this.parent = parent;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public void setState(GameContext state) {
		this.state = state;
	}
}
