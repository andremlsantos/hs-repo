package net.demilich.metastone.game.behaviour.mcts;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import net.demilich.metastone.game.GameContext;
import net.demilich.metastone.game.Player;
import net.demilich.metastone.game.actions.EndTurnAction;
import net.demilich.metastone.game.actions.GameAction;
import net.demilich.metastone.game.behaviour.Behaviour;
import net.demilich.metastone.game.cards.Card;
import net.demilich.metastone.utils.Tuple;

//isto � a tese
//ia do mcts
public class MctsBehaviour extends Behaviour {

	// name
	private static final String name = "MCTS";

	// heuristic
	private ProgressiveBias bias;
	private TreePolicy treePolicy;
	private DefaultPolicy defaultPolicy;

	// selection child criteria
	private static SelectAction selector;
	public static WinningSelectionCriteria criteria;

	/*
	 * to connect with the tree nodes when returning special action: discover or
	 * battlecry
	 */
	public static Node parentNode, returningActionNode, root = null;
	public static GameAction parentAction, lastReturnedAction;
	public static boolean isReturningAction = false;
	public static int playerID = 0;
	public static Policies actualPolice = Policies.NONE;
	private MctsAuxiliar auxiliar;
	
	//max turn's time of 75
	//but in average!!!!, 
	//we last 15s in a new execution step 
	//so we 75-15=60 assures that we dont reach the max time
	private long tStart;
	private double maxTurnTime = 60;
	private boolean isTurnAlreadyStarted = false;
	
	public MctsBehaviour() {
		//we read our mcts parameters
		auxiliar = new MctsAuxiliar(this);
		auxiliar.readConfigs();
	}

	@Override
	public String getName() {
		return name;
	}

	private void setupMcts(Player player) {
		// heuristic 1
		// auxiliar.setupHeuristicWeights(player);
		// IHeuristic heuristic = new StateScoreFunction(auxiliar.getStateFeatures(), auxiliar.getTradeFeatures());

		// heuristic 2
		auxiliar.requestTrainingData(player);
		IHeuristic heuristic = new ThreatBasedHeuristic(auxiliar.getWeights(), player.getId());
		
		//no heuristic - vanilha
		//IHeuristic heuristic = null;

		this.bias = new ProgressiveBias(heuristic);
		this.treePolicy = new Ucb(bias);

		// default policy + heuristic
		this.defaultPolicy = new RationalDefaultPolicy(heuristic, auxiliar.isDynamic(), auxiliar.getKPlayer(), auxiliar.getKOpponent(), auxiliar.getPercentage());
		
		// default policy + no heuristic - vanilha
		//this.defaultpolicy = new RandomDefaultPolicy();

		// select the candidate action from the tree
		MctsBehaviour.criteria = auxiliar.getCriteria();
		selector = new SelectAction(MctsBehaviour.criteria);
		
		//reset previous data
		root = null;
		lastReturnedAction = null;
	}

	@Override
	public List<Card> mulligan(GameContext context, Player player, List<Card> cards) {
		// we perform other setup
		setupMcts(player);

		// we reject cards with base manna cost > to baseManaCost
		int baseManaCost = 3;

		List<Card> discardedCards = new ArrayList<Card>();
		for (Card card : cards) {
			if (card.getBaseManaCost() > baseManaCost) {
				discardedCards.add(card);
			}
		}

		return discardedCards;
	}

	//retirnar uma unica acao
	//comeca o planeamento
	@Override
	public GameAction requestAction(GameContext context, Player player, List<GameAction> validActions) {
		//we are planning, so we set this flag to false
		MctsBehaviour.isReturningAction = false; 
		
		// set mcts player id
		MctsBehaviour.playerID = player.getId(); 

		//count the turn's time
		//we initiate the turn's time counter
		if(!isTurnAlreadyStarted) {
			this.isTurnAlreadyStarted = true;
			tStart = System.currentTimeMillis();
		}
		
		//if we only can perform the end turn, or we already achieved the maximum turn time
		//we perform the end turn
		if (validActions.size() == 1 || (System.currentTimeMillis() - tStart)/1000 > maxTurnTime) {			
			lastReturnedAction = validActions.get(0);
			this.isTurnAlreadyStarted = false;
			return validActions.get(0);
		}

		//otherwise, we plan!!
		GameAction action = null;
		
		if (!auxiliar.isContinueIterations()) {
			action = resetTree(context, player, validActions);
		} else {
			action = continueTree(context, player, validActions);
		}
		
		return action;
	}

	/*	TO CONNECT THE STATE WITH LOGIC
	 * EXTRA ACTIONS: DISCOVER, BATTLECRY, SPEEL
	 */
	public static GameAction requestExtraAction(GameContext context, List<GameAction> normalActions,
			List<GameAction> specialActions) {
		// discover action
		Node specialNode = new Node(context, specialActions); // spellUtils
		specialNode.setAction(MctsBehaviour.parentAction); // Summon
		specialNode.setParent(MctsBehaviour.parentNode); // parent

		// escolher special action + retorna-la
		Random randomizer = new Random();
		int actionIndex = randomizer.nextInt(specialActions.size());
		GameAction specialAction = specialActions.get(actionIndex);

		specialNode.getAvailableActions().remove(actionIndex);

		// update references
		MctsBehaviour.parentNode.getChildren().add(specialNode);
		MctsBehaviour.parentAction = specialAction;
		MctsBehaviour.parentNode = specialNode;

		return specialAction;
	}

	// we cant continue MCTS
	//we initialize the search from scratch
	private GameAction resetTree(GameContext context, Player player, List<GameAction> validActions) {
		root = new Node(context);

		for (int i = 0; i < auxiliar.getIterations(); i++) {
			Node child = treePolicy(root);
			defaultPolicy(child);
		}

		//we select the candidate action from the tree
		Node childSelected = selector.selectAction(root);
		lastReturnedAction = childSelected.getAction();
		
		//the child node selected is our new root node
		//for our next mcts' iteration
		root = childSelected;

		return lastReturnedAction;
	}

	// we continue MCTS
	//FROM the previous root node -> childSelected
	private GameAction continueTree(GameContext context, Player player, List<GameAction> validActions) {
		//but first we check if its possible to continue mcts
		if (auxiliar.canIterate(validActions, root)) {

			for (int i = 0; i < auxiliar.getIterations(); i++) {
				// System.out.println("continueTree " + i);
				Node child = treePolicy(root);
				defaultPolicy(child);
			}

			//we select the candidate action from the tree
			Node childSelected = selector.selectAction(root);
			lastReturnedAction = childSelected.getAction();
			
			//the child node selected is our new root node
			//for our next mcts' iteration
			root = childSelected;

			return lastReturnedAction;
		} else {
			return resetTree(context, player, validActions);
		}
	}

	//self explanatory 
	private Node treePolicy(Node v) {
		actualPolice = Policies.TREE;

		while (!v.isTerminal()) {
			if (!v.isFullyExpanded()) {
				return treePolicy.expand(v);
			} else {
				v = treePolicy.bestChild(v);
			}
		}
		return v;
	}

	//self explanatory
	private void defaultPolicy(Node v) {
		actualPolice = Policies.DEFAULT;

		Tuple<Integer, Integer> result = defaultPolicy.rollOut(v.getState(), auxiliar.getRollOut());
		backUp(v, result.getFirst(), result.getSecond());
	}

	//self explanatory
	private void backUp(Node v, int victories, int visits) {
		Node updated = v;

		while (updated != null) {
			updated.increaseVictories(victories);
			updated.increaseVisits(visits);

			updated = updated.getParent();
		}
	}
}
