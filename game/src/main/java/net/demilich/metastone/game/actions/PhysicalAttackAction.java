package net.demilich.metastone.game.actions;

import net.demilich.metastone.game.Attribute;
import net.demilich.metastone.game.GameContext;
import net.demilich.metastone.game.Player;
import net.demilich.metastone.game.behaviour.mcts.DefaultPolicy;
import net.demilich.metastone.game.behaviour.mcts.MctsBehaviour;
import net.demilich.metastone.game.entities.Actor;
import net.demilich.metastone.game.entities.Entity;
import net.demilich.metastone.game.entities.EntityType;
import net.demilich.metastone.game.targeting.EntityReference;
import net.demilich.metastone.game.targeting.TargetSelection;

public class PhysicalAttackAction extends GameAction {

	private final EntityReference attackerReference;
	private String name = "";

	public PhysicalAttackAction(EntityReference attackerReference) {
		setTargetRequirement(TargetSelection.ENEMY_CHARACTERS);
		setActionType(ActionType.PHYSICAL_ATTACK);
		this.attackerReference = attackerReference;
		this.name = "attacker" + attackerReference.getId();
	}

	@Override
	public boolean canBeExecutedOn(GameContext context, Player player, Entity entity) {
		// NUNCA ENTRA
		if (!super.canBeExecutedOn(context, player, entity)) {
			return false;
		}
		// SE NAO FORES HEROI
		if (entity.getEntityType() != EntityType.HERO) {
			return true;
		}
		Actor attacker = (Actor) context.resolveSingleTarget(attackerReference);
		return !attacker.hasAttribute(Attribute.CANNOT_ATTACK_HEROES);
	}

	@Override
	public void execute(GameContext context, int playerId) {
		Actor attacker = (Actor) context.resolveSingleTarget(attackerReference);
		Actor defender = (Actor) context.resolveSingleTarget(getTargetKey());

		try { 
			context.getLogic().fight(context.getPlayer(playerId), attacker, defender);
		} catch (Exception e) {
			System.out.println("PhysicalAttackAction cancelada, 48, execute()");
			//cancelamos a execucao da simulacao
			DefaultPolicy.simulation.cancel(true);
		}
	}

	public EntityReference getAttackerReference() {
		return attackerReference;
	}

	@Override
	public String getPromptText() {
		return "[Physical Attack]";
	}

	@Override
	public boolean isSameActionGroup(GameAction anotherAction) {
		if (anotherAction.getActionType() != getActionType()) {
			return false;
		}
		PhysicalAttackAction physicalAttackAction = (PhysicalAttackAction) anotherAction;

		return this.getAttackerReference().equals(physicalAttackAction.getAttackerReference());
	}

	@Override
	public String toString() {
		return String.format("%s Attacker: %s Defender: %s", getActionType(), attackerReference, getTargetKey());
	}

	public String getName() {
		return this.name;
	}
}
