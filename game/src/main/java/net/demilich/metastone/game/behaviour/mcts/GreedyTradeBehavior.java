package net.demilich.metastone.game.behaviour.mcts;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import net.demilich.metastone.game.GameContext;
import net.demilich.metastone.game.Player;
import net.demilich.metastone.game.actions.GameAction;
import net.demilich.metastone.game.behaviour.Behaviour;
import net.demilich.metastone.game.cards.Card;

public class GreedyTradeBehavior extends Behaviour {

	private IHeuristic heuristic = null;

	public GreedyTradeBehavior(IHeuristic heuristic) {
		this.heuristic = heuristic;
	}

	@Override
	public String getName() {
		return "Greedy my heuristic";
	}

	@Override
	public List<Card> mulligan(GameContext context, Player player, List<Card> cards) {
		if(this.heuristic == null)
			setupHeuristicWeights(player);
		
		List<Card> discardedCards = new ArrayList<Card>();
		for (Card card : cards) {
			if (card.getBaseManaCost() > 3) {
				discardedCards.add(card);
			}
		}
		return discardedCards;
	}

	@Override
	public GameAction requestAction(GameContext context, Player player, List<GameAction> validActions) {
		if (validActions.size() == 1) {
			return validActions.get(0);
		}

		GameAction bestAction = validActions.get(0);
		double bestScore = Double.NEGATIVE_INFINITY;

		for (GameAction action : validActions) {
			// simulate action
			double score = simulateAction(context.clone(), player, action, heuristic);

			if (score > bestScore) {
				bestScore = score;
				bestAction = action;
			} else if (score == bestScore) {
				long seed = System.nanoTime();
				Random randomizer = new Random(seed);
				int swap = randomizer.nextInt(2);
				
				if(swap == 1) {
					bestScore = score;
					bestAction = action;
				}		
			}
		}
		return bestAction;
	}
	
	private void setupHeuristicWeights(Player player) {
		System.out.println("setupHeuristicWeights");
		MctsAuxiliar auxiliar = new MctsAuxiliar();
		auxiliar.setupHeuristicWeights(player);
		
		StateScoreFunctionFeatures stateFeature = auxiliar.getStateFeatures();
		TradeFeatures tradeFeature = auxiliar.getTradeFeatures();
		
		IHeuristic heuristic = new StateScoreFunction(stateFeature, tradeFeature);
		this.heuristic = heuristic;
	}
}
