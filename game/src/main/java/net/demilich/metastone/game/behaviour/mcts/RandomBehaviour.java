package net.demilich.metastone.game.behaviour.mcts;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import net.demilich.metastone.game.GameContext;
import net.demilich.metastone.game.Player;
import net.demilich.metastone.game.actions.GameAction;
import net.demilich.metastone.game.behaviour.Behaviour;
import net.demilich.metastone.game.cards.Card;

//ia que escolhe acoes random
//usado pela vanilha na simulacao
public class RandomBehaviour extends Behaviour {

	private static final String name = "DefaultPolicyPlayerBehaviour";

	// heuristic
	private IHeuristic heuristic;
	private int kActions, minActions, percentage;
	private boolean isDynamic, isMcts;

	public RandomBehaviour(IHeuristic heuristc, boolean isDynamic, boolean isMcts, int minK, int percentage) {
		this.heuristic = heuristc;
		this.minActions = minK;
		this.kActions = minK;
		this.isDynamic = isDynamic;
		this.percentage = percentage;
		this.isMcts = isMcts;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public List<Card> mulligan(GameContext context, Player player, List<Card> cards) {
		return new ArrayList<>();
	}

	@Override
	public GameAction requestAction(GameContext context, Player player, List<GameAction> validActions) {
		setupK(validActions);

		if (isMcts)
			return playerPolicy(validActions, context, player);
		return opponentPolicy(validActions, context, player);
	}

	// with heuristic
	private GameAction playerPolicy(List<GameAction> validActions, GameContext state, Player player) {
		// result variables
		double heuristicValue = Double.NEGATIVE_INFINITY;
		GameAction bestHeuristicAction = null;

		List<GameAction> randomActions = DefaultPolicy.tounementSelection(validActions, kActions);

		for (GameAction gameAction : randomActions) {

			// state clone
			double score = simulateAction(state.clone(), player, gameAction, heuristic);

			if (score >= heuristicValue) {

				heuristicValue = score;
				bestHeuristicAction = gameAction;
			}
		}
		return bestHeuristicAction;
	}

	// without heuristic
	private GameAction opponentPolicy(List<GameAction> validActions, GameContext state, Player player) {
		double heuristicValue = Double.POSITIVE_INFINITY;
		GameAction worstHeuristicAction = null;
		
		List<GameAction> randomActions = DefaultPolicy.tounementSelection(validActions, kActions);

		for (GameAction gameAction : randomActions) {
			// state clone
			double score = simulateAction(state.clone(), player, gameAction, heuristic);
			
			if(score <= heuristicValue){
				heuristicValue = score;
				worstHeuristicAction = gameAction;
			}
		}
		
		return worstHeuristicAction;
	}

	private void setupK(List<GameAction> validActions) {
		int actionsCount = validActions.size();

		if (isDynamic) {
			int percentageK = Math.round((actionsCount * percentage) / 100);

			if (percentageK > minActions) {
				this.kActions = percentageK;
			} else {
				this.kActions = Math.min(actionsCount, minActions);
			}
		} else {
			this.kActions = Math.min(actionsCount, minActions);
		}
	}
}