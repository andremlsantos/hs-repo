package net.demilich.metastone.game.behaviour.mcts;

import java.util.Comparator;

public class ChromosomeComparor implements Comparator<Chromosome>{
	
	//para ordenar a populacao por fitness crescente
	//so e usado no metodo de selecao do rankeamento 
	@Override
	public int compare(Chromosome o1, Chromosome o2) {
		return (int)(o2.getFitness() - o1.getFitness());
	}
}
