package net.demilich.metastone.game.behaviour.mcts;

import net.demilich.metastone.game.Attribute;
import net.demilich.metastone.game.GameContext;
import net.demilich.metastone.game.Player;
import net.demilich.metastone.game.actions.GameAction;
import net.demilich.metastone.game.entities.minions.Minion;
import net.demilich.metastone.game.spells.desc.SpellArg;
import net.demilich.metastone.utils.Tuple;

/*
 * HEURISTICA 1
 * REPRESENTS BOARD CONTROL STRATEGY
 * */
public class StateScoreFunction implements IHeuristic {

	private StateScoreFunctionFeatures feature;	//5 METODOS DA HEURISTICA
	private TradeFeatures tradeFeatures;		//FORMA DE COMO CALCULAMOS O TRADE

	// for genetic training
	public StateScoreFunction(StateScoreFunctionFeatures feature, TradeFeatures tradeFeatures) {
		this.feature = feature;
		this.tradeFeatures = tradeFeatures;
	}

	@Override
	public double evaluate(GameContext state, GameAction action) {
		// id
		int playerId = MctsBehaviour.playerID;

		// get booth players
		Player player = state.getPlayer(playerId);
		Player opponent = state.getOpponent(player);

		// calc
		return MinionAdvantage(player, opponent) + ToughMinionAdvantage(player, opponent)
				+ HandAdvantage(player, opponent) + TradeAdvantage(player, opponent)
				+ BoardManaAdvantage(player, opponent);
	}

	@SuppressWarnings("static-access")
	private double MinionAdvantage(Player player, Player opponent) {
		return feature.MinionAdvantage.getWeight() * (player.getMinions().size() - opponent.getMinions().size());
	}

	@SuppressWarnings("static-access")
	private double ToughMinionAdvantage(Player player, Player opponent) {
		int toughtHp = 4;
		int toughtMinionsPlayer = 0, toughtMinionsOpponent = 0;

		for (Minion minion : player.getMinions()) {
			if (minion.getHp() >= toughtHp)
				toughtMinionsPlayer++;
		}

		for (Minion minion : opponent.getMinions()) {
			if (minion.getHp() >= toughtHp)
				toughtMinionsOpponent++;
		}

		return feature.ToughMinionAdvantage.getWeight() * (toughtMinionsPlayer - toughtMinionsOpponent);
	}

	@SuppressWarnings("static-access")
	private double HandAdvantage(Player player, Player opponent) {
		return feature.HandAdvantage.getWeight() * (player.getHand().getCount() - opponent.getHand().getCount());
	}

	/*
	 * CARD POINTS: hp+atk + (hp - atk)
	 */
	@SuppressWarnings("static-access")
	private double TradeAdvantage(Player player, Player opponent) {
		double tradeAdvantage = 0;
		int friendlyATK = 0, friendlyHP = 0, adversaryATK = 0, adversaryHP = 0; // minion
		int friendlyHeroATK = 0, friendlyHeroHP = 0, adversaryHeroATK = 0, adversaryHeroHP = 0; // hero

		// first we check if someone is dead
		if (player.getHero().getHp() < 1)
			return Double.NEGATIVE_INFINITY;
		if (opponent.getHero().getHp() < 1)
			return Double.POSITIVE_INFINITY;

		// MINION
		for (Minion minion : player.getMinions()) {
			Tuple<Double, Double> stats = getValueByAbility(minion);
			friendlyATK += stats.getFirst();
			friendlyHP += stats.getSecond();
		}

		// OPPONENT MINION
		for (Minion minion : opponent.getMinions()) {
			Tuple<Double, Double> stats = getValueByAbility(minion);
			adversaryATK += stats.getFirst();
			adversaryHP += stats.getSecond();
		}

		// HERO
		friendlyHeroHP += player.getHero().getArmor() + player.getHero().getHp();
		friendlyHeroATK += player.getHero().getAttack();

		// OPPONENT HERO
		adversaryHeroHP += opponent.getHero().getArmor() + opponent.getHero().getHp();
		adversaryHeroATK += opponent.getHero().getAttack();

		// WEAPON
		if (player.getHero().getWeapon() != null) {
			friendlyHeroATK += player.getHero().getWeapon().getAttack();
			friendlyHeroHP += player.getHero().getWeapon().getHp();
		}

		// OPPONENT WEAPON
		if (opponent.getHero().getWeapon() != null) {
			adversaryHeroATK += opponent.getHero().getWeapon().getAttack();
			adversaryHeroHP += opponent.getHero().getWeapon().getHp();
		}

		tradeAdvantage = friendlyATK * tradeFeatures.FriendlyATK.getWeight()
				+ friendlyHP * tradeFeatures.FriendlyHP.getWeight()
				+ friendlyHeroATK * tradeFeatures.FriendlyHeroATK.getWeight()
				+ friendlyHeroHP * tradeFeatures.FriendlyHeroHp.getWeight()
				- (adversaryATK * tradeFeatures.AdversaryATK.getWeight()
						+ adversaryHP * tradeFeatures.AdversaryHP.getWeight()
						+ adversaryHeroATK * tradeFeatures.AdversaryHeroATK.getWeight()
						+ adversaryHeroHP * tradeFeatures.AdversaryHeroHp.getWeight());

		return feature.TradeAdvantage.getWeight() * tradeAdvantage;
	}

	@SuppressWarnings("static-access")
	private double BoardManaAdvantage(Player player, Player opponent) {
		int playerMinionsMana = 0, opponentMinionsMana = 0;

		for (Minion minion : player.getMinions()) {
			playerMinionsMana += minion.getSourceCard().getBaseManaCost();
		}

		for (Minion minion : opponent.getMinions()) {
			opponentMinionsMana += minion.getSourceCard().getBaseManaCost();
		}

		return feature.BoardManaAdvantage.getWeight() * (playerMinionsMana - opponentMinionsMana);
	}

	/*
	 * AUX
	 * ajsutes que eu e prof pedro fizemos a la cart
	 * recomendo algoritmo que aprenda este pesos
	 * por exemplo, quando minion esta congelado, e como nao pode atacar, os seus pontos de ataque nao contam no trade da heuristica
	 */
	private Tuple<Double, Double> getValueByAbility(Minion minion) {
		double hp = minion.getHp();
		double atk = minion.getAttack();

		if (minion.hasAttribute(Attribute.SILENCED)) {
			// we dont look for other atributes
			return new Tuple<Double, Double>(atk, hp);
		}

		if (minion.hasAttribute(Attribute.STEALTH) || minion.hasAttribute(Attribute.IMMUNE)) { // ?
			hp = 10;
		}

		if (minion.hasAttribute(Attribute.DIVINE_SHIELD)) {
			hp += 3;
		}

		if (minion.hasAttribute(Attribute.DEATHRATTLES)) {
			hp += 0;
		}

		if (minion.hasAttribute(Attribute.TAUNT)) {
			hp *= 1.5;
		}
		if (minion.hasAttribute(Attribute.MEGA_WINDFURY)) {
			atk *= 3;
		}
		if (minion.hasAttribute(Attribute.WINDFURY)) {
			atk *= 1.5;
		}

		if (minion.hasAttribute(Attribute.ENRAGABLE)) {
			int actual_attack = minion.getAttack();
			int enrage = minion.getSpellTrigger().getSpell().getInt(SpellArg.ATTACK_BONUS, 0);

			if (actual_attack < minion.getBaseAttack() + enrage)
				atk += enrage * 0.5;
		}

		if (minion.hasAttribute(Attribute.UNLIMITED_ATTACKS)) {
			atk *= 3;
		}

		if (minion.hasAttribute(Attribute.FROZEN)) {
			atk *= 0;
		}

		return new Tuple<Double, Double>(atk, hp);
	}
}
