package net.demilich.metastone.game.behaviour.mcts;

import java.util.ArrayList;
import java.util.List;
import net.demilich.metastone.NotificationProxy;
import net.demilich.metastone.game.Player;
import net.demilich.metastone.game.actions.ActionType;
import net.demilich.metastone.game.actions.GameAction;
import net.demilich.metastone.game.behaviour.threat.FeatureVector;
import net.demilich.metastone.gui.mctsmode.GeneticDB;
import net.demilich.metastone.gui.mctsmode.MctsSetup;
import net.demilich.metastone.trainingmode.RequestTrainingDataNotification;
import net.demilich.metastone.trainingmode.TrainingData;

//auxiliar do mcts
public class MctsAuxiliar {

	MctsBehaviour mctsBehaviour;

	// CONFIGS
	private int difficulty = -1;		//temos diferentes niveis de dificuldade
	private int iterations = -1;		//n iteracoes	
	private int rollOut = -1;			//m simulacoes
	private int kMctsAction = -1;		//valor fixo de k para o mcts - k fixo
	private int kOpponentAction = -1;	//valor fixo de k para o oponente - k fixo
	private int[] percentage;			//valores k dinamicos para o mcts[0] e oponente[1] - k dinamico
	private boolean isDynamic;			//se usamos k fixo ou dinamico
	private boolean continueIterations;	//se fazemos o search tree reuse
	private WinningSelectionCriteria criteria;	//quando o budget termina, como selecionams a acao da arvore

	//heuristic1
	private StateScoreFunctionFeatures feature;	//parte 1 da nossa heuristica, contem os 5 metodos 
	private TradeFeatures tradeFeatures;		//parte 2 da heuristica, formula de como calculamos o trade de acordo formula prof pedro	
	
	//heuristic2
	private IHeuristic heuristic;
	private FeatureVector weights;	//pesos da heuristica 2
	
	public MctsAuxiliar() { }

	public MctsAuxiliar(MctsBehaviour mcts) {
		this.mctsBehaviour = mcts;
	}

	/*
	 * very important method,
	 * before we continue MCTS
	 * we need to check if we can continue
	 * 
	 * some actions in the tree are outdated!
	 * some actions in the tree are different than the current state! 
	 * */
	public boolean canIterate(List<GameAction> validActions, Node root) {
		if ((root == null && MctsBehaviour.lastReturnedAction == null)
				|| MctsBehaviour.lastReturnedAction.getActionType().equals(ActionType.END_TURN))
			return false;

		boolean canContinue = true;
		List<GameAction> treeActions = new ArrayList<>();

		for (int i = 0; i < root.getChildren().size(); i++) {
			if (root.getChildren().get(i).getAction() == null) {
				canContinue = false;
				break;
			} else {
				treeActions.add(root.getChildren().get(i).getAction());
			}
		}

		if (!canContinue)
			return false;

		boolean isPresent = true;
		int i = 0;

		while (isPresent && i < treeActions.size()) {
			isPresent = isActionPresent(treeActions.get(i), validActions);
			i++;
		}
		return isPresent;
	}

	public int getDifficulty() {
		return difficulty;
	}

	public int getIterations() {
		return iterations;
	}

	public int getKOpponent() {
		return kOpponentAction;
	}

	public int getKPlayer() {
		return kMctsAction;
	}

	public int[] getPercentage() {
		return percentage;
	}

	public int getRollOut() {
		return rollOut;
	}

	public StateScoreFunctionFeatures getStateFeatures() {
		return feature;
	}

	public TradeFeatures getTradeFeatures() {
		return tradeFeatures;
	}

	//auxiliar of canIterate()
	private boolean isActionPresent(GameAction action, List<GameAction> validActions) {
		for (int i = 0; i < validActions.size(); i++) {
			GameAction _action = validActions.get(i);

			int id1 = -1, id2 = -1, target1 = -1, target2 = -1;
			String name1 = null, name2 = null;

			//action information
			//we need to perform it, because the project architecture was not prepared 
			try {
				id1 = _action.getId(_action);
				id2 = action.getId(action);
				target1 = _action.getTarget(_action);
				target2 = action.getTarget(action);
				name1 = _action.getName(_action);
				name2 = action.getName(action);
			} catch (Exception e) {
				return false;
			}

			if (id1 == id2 && name1.equals(name2) && target1 == target2
					&& _action.getActionType().equals(action.getActionType())) {
				return true;
			}
		}
		return false;
	}

	public boolean isContinueIterations() {
		return continueIterations;
	}

	public boolean isDynamic() {
		return isDynamic;
	}

	//we read the mcts parameters
	//from json file
	public void readConfigs() {
		MctsSetup.getInstance().readConfigs();

		this.difficulty = MctsSetup.getInstance().getDificulty();
		this.setIterations(MctsSetup.getInstance().getIterations()[difficulty]);
		this.setRollOut(MctsSetup.getInstance().getRollOut()[difficulty]);

		this.setkMctsAction(MctsSetup.getInstance().getkActions()[0]);
		this.setkOpponentAction(MctsSetup.getInstance().getkActions()[1]);
		this.setContinueIterations(MctsSetup.getInstance().getIterations()[3] == 1 ? true : false);
		this.setPercentage(MctsSetup.getInstance().getkActions()[3], MctsSetup.getInstance().getkActions()[4]);
		this.setDynamic(MctsSetup.getInstance().getkActions()[2] == 1 ? true : false);
		
		ArrayList<WinningSelectionCriteria> criterias = new ArrayList<>();
		criterias.add(WinningSelectionCriteria.MaxChild);
		criterias.add(WinningSelectionCriteria.MaxRobustChild);
		criterias.add(WinningSelectionCriteria.RobustChild);
		criterias.add(WinningSelectionCriteria.SecureChild);
		
		this.setCriteria(criterias.get(MctsSetup.getInstance().getSelectionCriteria()));
	}

	public void setContinueIterations(boolean continueIterations) {
		this.continueIterations = continueIterations;
	}

	public void setDifficulty(int difficulty) {
		this.difficulty = difficulty;
	}

	public void setDynamic(boolean isDynamic) {
		this.isDynamic = isDynamic;
	}

	public void setFeature(StateScoreFunctionFeatures feature) {
		this.feature = feature;
	}

	public void setIterations(int iterations) {
		this.iterations = iterations;
	}

	public void setkMctsAction(int kMctsAction) {
		this.kMctsAction = kMctsAction;
	}

	public void setkOpponentAction(int kOpponentAction) {
		this.kOpponentAction = kOpponentAction;
	}

	public void setPercentage(int playerPercentage, int opponentPercentage) {
		int[] percentage = { playerPercentage, opponentPercentage };
		this.percentage = percentage;
	}

	public void setRollOut(int rollOut) {
		this.rollOut = rollOut;
	}

	public void setTradeFeatures(TradeFeatures tradeFeatures) {
		this.tradeFeatures = tradeFeatures;
	}

	//heuristic 1 data
	@SuppressWarnings("static-access")
	public void setupHeuristicWeights(Player player) {
		String deckName = player.getDeckName();

		GeneticDB.getInstance().readGeneticDeck(deckName);

		double[] weights = GeneticDB.getInstance().getValues();
		int index = 0;

		ArrayList<String> deckNames = GeneticDB.getInstance().getGeneticDecksNames();

		for (String name : deckNames) {
			if (name.equals(deckName)) {
				for (int i = 0; i < feature.values().length; i++) {
					this.feature.values()[i].setWeight(weights[index++]);
				}

				for (int i = 0; i < tradeFeatures.values().length; i++) {
					this.tradeFeatures.values()[i].setWeight(weights[index++]);
				}
			}
		}
	}
	
	//heuristic 2 data
	public void requestTrainingData(Player player) {
		if (heuristic != null) {
			return;
		}

		RequestTrainingDataNotification request = new RequestTrainingDataNotification(player.getDeckName(), this::answerTrainingData);
		NotificationProxy.notifyObservers(request);
	}
	
	private void answerTrainingData(TrainingData trainingData) {
		this.setWeights(trainingData != null ? trainingData.getFeatureVector() : FeatureVector.getFittest());
	}

	public FeatureVector getWeights() {
		return weights;
	}

	public void setWeights(FeatureVector weights) {
		this.weights = weights;
	}

	public WinningSelectionCriteria getCriteria() {
		return criteria;
	}

	public void setCriteria(WinningSelectionCriteria criteria) {
		this.criteria = criteria;
	}
}
