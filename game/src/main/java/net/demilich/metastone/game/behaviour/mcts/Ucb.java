package net.demilich.metastone.game.behaviour.mcts;

public class Ucb extends TreePolicy {

	public Ucb(ProgressiveBias bias) {
		super(bias);
	}

	@Override
	public double evaluate(Node child, Node parent, double cp) {
		return child.getVictories() / child.getVisits() + cp * Math.sqrt( (2 * Math.log(parent.getVisits()) ) / child.getVisits());
	}
}
