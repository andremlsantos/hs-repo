package net.demilich.metastone.game.behaviour.mcts;

import net.demilich.metastone.game.GameContext;
import net.demilich.metastone.game.Player;
import net.demilich.metastone.game.behaviour.PlayRandomBehaviour;
import net.demilich.metastone.game.cards.Card;
import net.demilich.metastone.game.cards.CardCollection;
import net.demilich.metastone.game.decks.Deck;
import net.demilich.metastone.game.targeting.CardLocation;
import net.demilich.metastone.utils.Tuple;

/*
 * its based on random actions selection
 * */
public class RandomDefaultPolicy extends DefaultPolicy {
	
	public RandomDefaultPolicy() {
		super();
		isSampling = true;
		decksSampling = new DecksSampling();
	}

	/*
	 * HIDDEN INFORMATION PROBLEM SETUP we setup the sampling behavior AND
	 * RETURN A NEW OPPONENT PLAYER WITH THE SELECTED DECK
	 */
	@Override
	protected Tuple<Player, Player> setupSampling(GameContext state, Player player, Player opponent) {
		Player opponentClone = opponent.clone();
		Player mctsClone = player.clone();

		// we invoke the deck sampling and execute according the opponents
		// minions and graveyard
		Deck sampleDeck = decksSampling.execute(opponentClone);

		// from the sample deck, we remove the cards already played
		decksSampling.removePlayedCards(opponentClone, sampleDeck);

		// cards count
		int nHandCards = Math.min(opponentClone.getHand().getCount(), sampleDeck.getCards().getCount());

		CardCollection hand = new CardCollection();
		CardCollection deck = new CardCollection();

		deck.addAll(sampleDeck.getCards());
		deck.shuffle();

		// we set the hand cards
		for (int i = 0; i < nHandCards; i++) {
			Card handCard = deck.removeFirst();
			handCard.setOwner(opponent.getHand().get(i).getOwner());
			handCard.setLocation(CardLocation.HAND);
			handCard.setId(opponent.getHand().get(i).getId());

			hand.add(handCard);
		}

		int nDeckCards = Math.min(deck.getCount(), opponent.getDeck().getCount());

		for (int i = 0; i < nDeckCards; i++) {
			deck.get(i).setLocation(CardLocation.DECK);
			deck.get(i).setId(opponent.getDeck().get(i).getId());
		}

		// COIN CARD
		// carta the coin esta presente na mao do oponente!
		// tiramos a ultima carta adicionda a mao, e colocamos no deck
		if (nDeckCards + nHandCards > 30 && hand.getCount() > 0) {
			Card extraCard = hand.removeFirst();
			extraCard.setLocation(CardLocation.DECK);
			deck.add(extraCard);

		}

		opponentClone.setDeck(deck);
		opponentClone.setHand(hand);

		return new Tuple<Player, Player>(mctsClone, opponentClone);
	}

	@Override
	protected void setupBehavior(Player player, Player opponent) {
		if (mctsPolicy == null && opponentPolicy == null) {
			mctsPolicy = new PlayRandomBehaviour();  
			opponentPolicy = new PlayRandomBehaviour(); 
		}
	}
}