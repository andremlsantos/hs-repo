package net.demilich.metastone.game.behaviour.mcts;

/*
 * EXPERT KNOWLEDGE
 * 5 metodos da heuristica
 * */
public enum StateScoreFunctionFeatures {
	
	//feature weight
	MinionAdvantage(1), 
	ToughMinionAdvantage(1),
	HandAdvantage(1),
	TradeAdvantage(1),
	BoardManaAdvantage(1);
	
	double weight;
	
	private StateScoreFunctionFeatures(double weight) {
		this.weight = weight;
	}
	
	public double getWeight() {
		return weight;
	}
	
	void setWeight(double weight) {
		this.weight = weight;
	}
}
