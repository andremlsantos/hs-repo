package net.demilich.metastone.game.behaviour.mcts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import net.demilich.metastone.game.GameContext;
import net.demilich.metastone.game.decks.Deck;
import net.demilich.metastone.utils.Tuple;

public class Genetic {

	// fitness constants
	public static double lastRatio = 0; // roulette selection
	public static int chromossomeSize = StateScoreFunctionFeatures.values().length + TradeFeatures.values().length; // #

	private int total = 0; // rank selection
	// configurations
	private int populationSize;
																													// WEIGHTS
	private double minWeight = 0, maxWeight = 10; // WEIGHTS

	private float crossoverProbability, mutationProbability;
	private int nGames, individualGames, nDecks; // fitness criteria

	private Random randomizer = new Random();
	private GeneticTrade trade;
	private List<Deck> geneticDecks;
	
	// RESULTS
	private Chromosome[] population;
	private List<Chromosome> rankedPopulation;
	private Selection selectionModel;
	private int rankSum = 0;

	public Genetic(GeneticTrade trade) {
		this.trade = trade;
	}

	/*
	 * FITNESS
	 */
	private void calcFitness() {
		total = 0;

		for (int i = 0; i < populationSize; i++) {
			population[i].clear();
			total += fitness(population[i]);
		}

		// ROULLETTE SELECTION
		if (selectionModel.equals(Selection.ROULETTE)) {
			for (int i = 0; i < populationSize; i++) {
				if (i > 0)
					fitnessRatio(population[i], population[i - 1], total);
				else
					fitnessRatio(population[i], null, total);
			}
		}

		// RANK SELECTION
		if (selectionModel.equals(Selection.RANK)) {
			rankedPopulation = Arrays.asList(population);
			rankSum = 0;
			Collections.sort(rankedPopulation, new ChromosomeComparor());

			for (int i = 0; i < populationSize; i++) {

				int rank = populationSize - i;
				rankedPopulation.get(i).setRank(rank);
				rankSum += rank;
			}
		}
	}

	/*
	 * SELECTION
	 */
	private Chromosome selection(Selection sel) {
		Chromosome chromosome = null;

		switch (sel) {
		case ROULETTE:
			chromosome = rouletteWheelSelection();
			break;
		case TOURNAMENT:
			chromosome = tournamentSelection();
			break;
		case RANK:
			chromosome = rankSelection();
			break;
		case RANDOM:
			chromosome = randomSelection();
			break;
		default:
			break;
		}

		return chromosome;
	}
	
	/*
	 * SELECTION based
	 */
	private Chromosome randomSelection() {
		int index = randomizer.nextInt(populationSize);
		return population[index];
	}

	/*
	 * SELECTION based
	 */
	private Chromosome rankSelection() {
		Chromosome chromosome = rankedPopulation.get(populationSize - 1);
		double r = randomizer.nextFloat();
		double cj_1 = 0;

		for (Chromosome ranked : rankedPopulation) {

			double probRank = (double) ranked.getRank() / rankSum;
			double cj = cj_1 + probRank;

			if (r <= cj) {
				chromosome = ranked;
				break;
			} else {
				cj_1 += probRank;
			}
		}

		return chromosome;
	}

	/*
	 * SELECTION based
	 */
	private Chromosome rouletteWheelSelection() {
		int index, i = 0;
		boolean notFound = true;
		Chromosome result = population[0];

		if (total == 0)
			return population[randomizer.nextInt(populationSize)];

		index = randomizer.nextInt((int) lastRatio);

		while (notFound) {
			double[] fitnessRatio = null;
			try {
				fitnessRatio = population[i].getFitnessRatio();
			} catch (Exception e) {
				notFound = false;
			}

			if (fitnessRatio[0] <= index && fitnessRatio[1] >= index) {
				result = population[i];
				notFound = false;
			}
			i++;
		}

		return result;
	}
	
	/*
	 * CROSSOVER based on single point
	 */
	private Tuple<Chromosome, Chromosome> crossover(Chromosome a, Chromosome b) {
		Tuple<Chromosome, Chromosome> result = null;
		float minBound = 0.0f;
		float maxBound = 1f;

		float prob = randomizer.nextFloat() * (maxBound - minBound) + minBound;

		if (prob <= crossoverProbability) {

			int crossoverPoint = randomizer.nextInt(chromossomeSize);
			result = executeCrossover(a, b, crossoverPoint);

		} else {
			// clone
			Chromosome chromosomeOne = a.clone();
			Chromosome chromosomeTwo = b.clone();

			result = new Tuple<Chromosome, Chromosome>(chromosomeOne, chromosomeTwo);
		}

		return result;
	}
	
	//auxiliar que executa crossover
	private Tuple<Chromosome, Chromosome> executeCrossover(Chromosome a, Chromosome b, int index) {
		double[] result1 = new double[chromossomeSize];
		double[] result2 = new double[chromossomeSize];

		for (int i = 0; i < chromossomeSize; i++) {
			if (i < index) {
				// equal
				result1[i] = a.getValues()[i];
				result2[i] = b.getValues()[i];

			} else {
				// swap
				result1[i] = b.getValues()[i];
				result2[i] = a.getValues()[i];
			}
		}

		Chromosome chromosomeOne = a.clone();
		chromosomeOne.setValues(result1);

		Chromosome chromosomeTwo = b.clone();
		chromosomeTwo.setValues(result2);

		return new Tuple<Chromosome, Chromosome>(chromosomeOne, chromosomeTwo);
	}

	//calculo do fitness
	//uso threads por otimizacao, comeco todos os k jogos ao esmo tempo
	//pelas minhas contas, optimizacao na ordem dos 30 por cento, contei tempo e fiz contas
	//procurar por executor java thread no google
	private int fitness(Chromosome chromesone) {
		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {
				int cores = 5;
				ExecutorService executor = Executors.newFixedThreadPool(cores);

				List<Future<Void>> futures = new ArrayList<Future<Void>>();

				for (int i = 0; i < nDecks; i++) {
					Deck deck2 = geneticDecks.get(i);
					PlayDeckTask task = new PlayDeckTask(individualGames, chromesone);
					task.setOpponent(trade.getBehaviour());
					task.setTrade(trade);
					task.setDeck1(trade.getTrainingDeck());
					task.setDeck2(deck2);	
					
					Future<Void> future = executor.submit(task);
					futures.add(future);
				}

				for (Future<Void> future : futures) {
					try {
						future.get();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				try {
					executor.shutdown();
					executor.awaitTermination(Integer.MAX_VALUE, TimeUnit.SECONDS);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});

		t.setDaemon(true);
		t.start();
		try {
			//wait for all to end
			t.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		//System.out.println();

		return (int) chromesone.getFitness();
	}

	private void fitnessRatio(Chromosome chromesone, Chromosome previousChromosome, int total) {
		double ratio = 0;

		if (total != 0)
			ratio = (chromesone.getFitness() * 100) / total;

		chromesone.setFitnessRatio(ratio, total, previousChromosome);
	}

	/*
	 * MUTATION based on single point
	 */
	private void mutation(Chromosome chromesone) {
		float minBound = 0.0f;
		float maxBound = 1f;

		float prob = randomizer.nextFloat() * (maxBound - minBound) + minBound;

		if (prob <= mutationProbability) {

			double[] genes = chromesone.getValues();
			int indexGene = randomizer.nextInt(chromossomeSize);

			genes[indexGene] = randomizer.nextDouble() * (maxWeight - minWeight) + minWeight;
			chromesone.setValues(genes);
		}
	}
	
	/*
	 * POPULATION
	 */
	public void generatePopulation() {
		for (int k = 0; k < populationSize; k++) {
			double[] values = new double[chromossomeSize];

			// set the data
			for (int i = 0; i < chromossomeSize; i++) {
				values[i] = randomizer.nextDouble() * (maxWeight - minWeight) + minWeight;
			}

			Chromosome chromosome = new Chromosome();
			chromosome.setChromosomeLabel("X" + k);
			chromosome.setValues(values);

			population[k] = chromosome;
		}
	}

	public float getCrossoverProbability() {
		return crossoverProbability;
	}

	public List<Deck> getGeneticDecks() {
		return geneticDecks;
	}

	/*
	 * private int fitness(Chromosome chromesone) { Deck deck2 = null;
	 * 
	 * // long seed = System.nanoTime(); // Collections.shuffle(geneticDecks,
	 * new Random(seed));
	 * 
	 * int totalVictories = 0; GameContext state = null;
	 * 
	 * for (int i = 0; i < totalCicles; i++) { int individualVictories = 0;
	 * 
	 * for (int j = 0; j < individualGames; j++) { if (j == 0) { deck2 =
	 * geneticDecks.get(i); state = trade.createGame(chromesone,
	 * trade.getBehaviour(), trade.getDeck1(), deck2); } GameContext game =
	 * state.clone(); game.play();
	 * 
	 * if (game.getWinningPlayerId() == MctsBehaviour.playerID)
	 * individualVictories++; game.dispose(); }
	 * 
	 * Tuple<String, Integer> stat = new Tuple<String, Integer>(deck2.getName(),
	 * individualVictories); chromesone.getStats().insertValue(stat);
	 * chromesone.getStats().setDeck1(trade.getDeck1().getName());
	 * chromesone.getStats().setDeck1FileName(trade.getDeck1().getFilename());
	 * totalVictories += individualVictories; }
	 * 
	 * chromesone.setFitness(totalVictories); return totalVictories; }
	 */

	public float getMutationProbability() {
		return mutationProbability;
	}
	
	public int getnGames() {
		return nGames;
	}

	public Chromosome[] getPopulation() {
		return population;
	}

	public int getPopulationSize() {
		return populationSize;
	}

	public Selection getSelectionModel() {
		return selectionModel;
	}

	//loop infinito para treinar baralhos
	//enquanto nao fizzermos todas as iteracoes ou populacoes, estamos aqui
	public void loop() {
		while (trade.getIterations() > 0) {
			Chromosome[] newPopulation = new Chromosome[populationSize];
			calcFitness();
			int index = 0;

			for (int i = 0; i < populationSize / 2; i++) {
				Chromosome a = selection(selectionModel);
				Chromosome b = selection(selectionModel);

				Tuple<Chromosome, Chromosome> newChromosomes = crossover(a, b);

				a = newChromosomes.getFirst();
				b = newChromosomes.getSecond();

				mutation(a);
				mutation(b);

				newPopulation[index++] = a;
				newPopulation[index++] = b;
			}
			population = newPopulation;
			trade.getBest(population);
		}
		trade.printBestResult(true, true, chromossomeSize, trade.getBestResult());
	}

	/*
	 * AUX
	 */
	private void normalizeChromossome(Chromosome chromesone, boolean first, boolean second) {
		double sum1 = 0, sum2 = 0;
		double[] values = new double[chromossomeSize];

		// set the data
		for (int i = 0; i < chromossomeSize / 2; i++) {
			values[i] = Math.abs(randomizer.nextDouble() * (maxWeight - minWeight) + minWeight);
			values[i + chromossomeSize / 2] = randomizer.nextDouble() * (maxWeight - minWeight) + minWeight;

			if (first)
				sum1 += Math.abs(values[i]);
			if (second)
				sum2 += Math.abs(values[i + chromossomeSize / 2]);
		}

		// we normalize data
		for (int i = 0; i < chromossomeSize / 2; i++) {
			if (first)
				values[i] = values[i] / sum1;
			if (second)
				values[i + chromossomeSize / 2] = values[i + 4] / sum2;
		}

		chromesone.setValues(values);
	}

	public void saveData(int populationSize, int nGames, float crossover, float mutation, Selection selection) {
		this.populationSize = populationSize;
		this.nGames = nGames;
		this.crossoverProbability = crossover;
		this.mutationProbability = mutation;
		this.population = new Chromosome[populationSize];
		this.selectionModel = selection;
	}

	public void setCrossoverProbability(float crossoverProbability) {
		this.crossoverProbability = crossoverProbability;
	}

	public void setGeneticDecks(List<Deck> geneticDecks) {
		this.geneticDecks = geneticDecks;
		this.nDecks = this.geneticDecks.size();
		this.individualGames = nGames / nDecks;
	}
	
	public void setMutationProbability(float mutationProbability) {
		this.mutationProbability = mutationProbability;
	}

	public void setnGames(int nGames) {
		this.nGames = nGames;
	}

	public void setPopulation(Chromosome[] population) {
		this.population = population;
	}
	
	public void setPopulationSize(int populationSize) {
		this.populationSize = populationSize;
	}

	public void setSelectionModel(Selection selectionModel) {
		this.selectionModel = selectionModel;
	}
	
	/*
	 * SELECTION based
	 */
	private Chromosome tournamentSelection() {
		List<Chromosome> selection = new ArrayList<>();
		int k = populationSize / 4, i = 0;

		while (i < k) {
			int index = randomizer.nextInt(populationSize);
			Chromosome selected = population[index];

			if (!selection.contains(selected)) {
				selection.add(selected);
				i++;
			}
		}

		// get best of selection
		double bestResult = Double.NEGATIVE_INFINITY;
		Chromosome result = null;

		for (Chromosome selected : selection) {
			if (selected.getFitness() > bestResult) {
				result = selected;
				bestResult = selected.getFitness();
			} else if (selected.getFitness() == bestResult) {
				int swap = randomizer.nextInt(2);
				if (swap == 1) {
					result = selected;
					bestResult = selected.getFitness();
				}
			}
		}
		return result;
	}
}
