package net.demilich.metastone.game.behaviour.mcts;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import net.demilich.metastone.GameNotification;
import net.demilich.metastone.game.GameContext;
import net.demilich.metastone.game.Player;
import net.demilich.metastone.game.behaviour.Behaviour;
import net.demilich.metastone.game.behaviour.IBehaviour;
import net.demilich.metastone.game.cards.Card;
import net.demilich.metastone.game.cards.CardCatalogue;
import net.demilich.metastone.game.cards.CardSet;
import net.demilich.metastone.game.cards.HeroCard;
import net.demilich.metastone.game.decks.Deck;
import net.demilich.metastone.game.decks.DeckFormat;
import net.demilich.metastone.game.entities.heroes.HeroClass;
import net.demilich.metastone.game.gameconfig.PlayerConfig;
import net.demilich.metastone.game.logic.GameLogic;
import net.demilich.metastone.utils.Tuple;
import net.demilich.nittygrittymvc.SimpleCommand;
import net.demilich.nittygrittymvc.interfaces.INotification;

//isto liga a UI com o algortimo genetico
//deve de ser assim devido aos padroes de desenho
public class GeneticTrade extends SimpleCommand<GameNotification> {

	public static boolean normalizeFirst = true, normalizeSecond = true;

	private int iterations, base_iterations, counter = 0, generation = 0;
	// context
	private Genetic genetic = new Genetic(this);
	private Behaviour behaviour;				//ia do jogador 1 do algorimo genetico
	private HeroClass heroClass1, heroClass2;
	private PlayerConfig player1Config, player2Config;

	private List<Deck> geneticDecks, allDecks;
	private Deck deck1 = null;

	
	private Chromosome bestResult = new Chromosome();
	
	private double[] weights = new double[StateScoreFunctionFeatures.values().length + TradeFeatures.values().length];

	// timer
	private long simulationStart, simulationEnd;
	//we stop the genetic after k maxConvergenceRetries 
	private int maxConvergenceRetries, actualRetries = 0;

	// heuristic weights 
	//part 1
	private StateScoreFunctionFeatures stateFeatures;
	//part 2
	private TradeFeatures trade;

	private Random randomizer = new Random();

	/*
	 * AUX methods
	 */
	public GameContext createGame(Chromosome chromesone, Behaviour opponentBehaviour, Deck deck1, Deck deck2) {
		DeckFormat deckFormat = new DeckFormat();

		for (CardSet set : CardSet.values()) {
			deckFormat.addSet(set);
		}

		// create trade based on chromosome
		IHeuristic heuristic = createHeuristic(chromesone);

		// PLAYER 1
		heroClass1 = deck1.getHeroClass();
		player1Config = new PlayerConfig(deck1, new GreedyTradeBehavior(heuristic));

		player1Config.setName("Player 1 - Greedy");
		player1Config.setHeroCard(getHeroCardForClass(heroClass1));

		// PLAYER 2
		heroClass2 = deck2.getHeroClass();
		player2Config = new PlayerConfig(deck2, opponentBehaviour);

		player2Config.setName("Player 2 - GENETIC'S OPPONENT");
		player2Config.setHeroCard(getHeroCardForClass(heroClass2));

		Player player1 = new Player(player1Config);
		Player player2 = new Player(player2Config);

		return new GameContext(player1, player2, new GameLogic(), deckFormat);
	}

	// create heuristic based on chromosome features
	@SuppressWarnings("static-access")
	private IHeuristic createHeuristic(Chromosome chromesone) {
		int index = 0;

		for (int i = 0; i < stateFeatures.values().length; i++) {
			stateFeatures.values()[i].setWeight(chromesone.getValues()[index++]);
		}

		for (int i = 0; i < trade.values().length; i++) {
			trade.values()[i].setWeight(chromesone.getValues()[index++]);
		}

		IHeuristic heuristic = new StateScoreFunction(stateFeatures, trade);
		return heuristic;
	}

	//padrao desenho mediator
	@SuppressWarnings("unchecked")
	@Override
	public void execute(INotification<GameNotification> notification) {
		switch (notification.getId()) {
		case RECEIVE_DECKS:
			saveDecks((List<Deck>) notification.getBody());
			break;
		case SIMULATE_GENETIC:
			Object[] configs = (Object[]) notification.getBody();

			setParams(Integer.parseInt((String) configs[0]), Integer.parseInt((String) configs[1]),
					Integer.parseInt((String) configs[2]), Float.parseFloat((String) configs[3]),
					Float.parseFloat((String) configs[4]), Integer.parseInt((String) configs[5]),
					(IBehaviour) configs[6], (Selection) configs[7], (ComboBox<String>) configs[8]);
			run();
			break;
		default:
			break;
		}
	}

	public Behaviour getBehaviour() {
		return behaviour;
	}

	//obter o melhor cromossoma de uma populacao de acordo fitness
	//melhor local e global
	public void getBest(Chromosome[] population) {
		Chromosome bestLocalChromosome = null;
		double bestLocalResult = Double.NEGATIVE_INFINITY;
		
		// best local
		for (int i = 0; i < genetic.getPopulationSize(); i++) {
			if (genetic.getPopulation()[i].getFitness() > bestLocalResult) {
				bestLocalResult = genetic.getPopulation()[i].getFitness();
				bestLocalChromosome = genetic.getPopulation()[i].clone();
			} else if (genetic.getPopulation()[i].getFitness() == bestLocalResult) {
				int swap = randomizer.nextInt(2);
				if (swap == 1) {
					bestLocalResult = genetic.getPopulation()[i].getFitness();
					bestLocalChromosome = genetic.getPopulation()[i].clone();
				}
			}
		}

		// best global
		if (bestLocalChromosome.getFitness() > bestResult.getFitness()) {
			bestResult = bestLocalChromosome.clone();
			printBestResult(false, true, genetic.chromossomeSize, bestResult); 
		} else {
			printBestResult(false, false, genetic.chromossomeSize, bestLocalChromosome);
		}

		if (isConvergence(bestLocalChromosome.clone())) {
			actualRetries++;
			//System.out.println("Convergence " + actualRetries);
		} else {
			actualRetries = 0;
			//System.out.println("Convergence " + actualRetries);
		}
		
		this.weights = bestLocalChromosome.clone().getValues();

		if (actualRetries == maxConvergenceRetries) {
			iterations--;
			
			Tuple<Integer, Integer> progress = new Tuple<>(++counter, base_iterations);
			getFacade().sendNotification(GameNotification.SIMULATION_PROGRESS_UPDATE, progress);

			if(iterations > 0) {
				genetic.generatePopulation();
				System.out.println("\n\n-- Nova itera�ao --");
				printPopulation(population, genetic.chromossomeSize);
			}
			
			actualRetries = 0;
			generation = 0;
		} else {
			generation++;
		}
	}

	public Chromosome getBestResult() {
		return bestResult;
	}

	public List<Deck> getGeneticDecks() {
		return this.geneticDecks;
	}

	private HeroCard getHeroCardForClass(HeroClass heroClass) {
		for (Card card : CardCatalogue.getHeroes()) {
			HeroCard heroCard = (HeroCard) card;
			if (heroCard.getHeroClass() == heroClass) {
				return heroCard;
			}
		}
		return null;
	}

	private HeroClass getHeroClass() {
		HeroClass randomClass = HeroClass.ANY;
		HeroClass[] values = HeroClass.values();
		while (!randomClass.isBaseClass()) {
			randomClass = values[ThreadLocalRandom.current().nextInt(values.length)];
		}
		return randomClass;
	}

	public int getIterations() {
		return iterations;
	}

	private Deck getRandomDeck(HeroClass heroClass) {
		List<Deck> heroDecks = new ArrayList<>();

		for (Deck deck : geneticDecks) {
			if (deck.getHeroClass().equals(heroClass))
				heroDecks.add(deck);
		}

		int howMany = heroDecks.size();
		int index = randomizer.nextInt(howMany);

		return heroDecks.get(index);
	}

	public Deck getTrainingDeck() {
		return deck1;
	}

	private boolean isConvergence(Chromosome chromosome) {
		double[] chromosomeWeights = chromosome.getValues();
		
		boolean isConvergence = true;
		DecimalFormat decimalFormat = new DecimalFormat("#.##");

		for (int i = 0; i < chromosomeWeights.length; i++) {
			float a = Float.valueOf(decimalFormat.format(chromosomeWeights[i]));
			float b = Float.valueOf(decimalFormat.format(weights[i]));

			isConvergence = true;
			
			if (a != b) {
				isConvergence = false;
				break;
			}
		}

		return isConvergence;
	}

	//imprimir a melhor solucao no ecra
	//quando terminamos, guardamos resultados em json com padrao dersenho facade
	public void printBestResult(boolean isFinal, boolean isGlobal, int chromossomeSize, Chromosome chromosome) {
		if (isFinal) {
			getFacade().sendNotification(GameNotification.HIDE_SIMULATION_PROGRESS_UPDATE);
			getFacade().sendNotification(GameNotification.PRINT_GENETIC_WEIGHTS, bestResult);

			Object[] configs = { genetic.getPopulationSize(), genetic.getnGames(), this.base_iterations,
					genetic.getCrossoverProbability(), genetic.getMutationProbability(),
					genetic.getSelectionModel().name(), bestResult.getValues(), (int) bestResult.getFitness(),
					bestResult.getFitnessRatio(), bestResult.getRank(), bestResult.getStats(),
					this.behaviour.getName() };

			getFacade().sendNotification(GameNotification.SAVE_GENETIC_WEIGHTS, configs);

			// TOTAL SIMULATION TIME
			double totalSimulationTime;
			simulationEnd = System.currentTimeMillis();
			totalSimulationTime = (simulationEnd - simulationStart) / 1000;
			System.out.println("\n\nTOTAL TIME " + totalSimulationTime + "s\n\n");

			System.out.println("\n----> TOTAL-BEST");
			printChromosome(chromosome, chromossomeSize);
		} else if (!isGlobal) {
			System.out.println("\n BEST LOCAL: generation " + generation);
			printChromosome(chromosome, chromossomeSize);
		} else {
			System.out.println("\n----> BEST GLOBAL: generation " + generation);
			printChromosome(chromosome, chromossomeSize);
		}
	}

	public void printChromosome(Chromosome chromosome, int chromossomeSize) {
		DecimalFormat decimalFormat = new DecimalFormat("#.##");

		for (int i = 0; i < chromossomeSize; i++) {
			if (i == 0)
				System.out.print("[" + String.valueOf(decimalFormat.format(chromosome.getValues()[i])));
			else if (i == chromossomeSize - 1)
				System.out.print(", " + String.valueOf(decimalFormat.format(chromosome.getValues()[i])) + "] - FIT: "
						+ chromosome.getFitness());
			else
				System.out.print(", " + String.valueOf(decimalFormat.format(chromosome.getValues()[i])));
		}
		System.out.println();
		if (chromosome.getStats().getValues().size() > 0) {
			for (int i = 0; i < geneticDecks.size(); i++) {
				System.out.println("DECK" + i + " " + chromosome.getStats().getValues().get(i).getFirst() + " - "
						+ chromosome.getStats().getValues().get(i).getSecond());
			}
		}
	}
	
	public void printPopulation(Chromosome[] population, int chromossomeSize) {
		System.out.println("-- Populacao inicial, RESTART " + counter + " --");
		for (Chromosome chromosome : population) {
			printChromosome(chromosome, chromossomeSize);
		}
	}

	//iniicia algoritmo genetico
	private void run() {
		genetic.generatePopulation();
		System.out.println("DECK SELECTED: " + deck1.getName() + "; RESTARTS " + getIterations()
				+ "; POPULATION_SIZE " + genetic.getPopulationSize() + "; TOTAL GAMES " + genetic.getnGames()
				+ "; SELECTION " + genetic.getSelectionModel() + "; CROSSOVER " + genetic.getCrossoverProbability()
				+ "f; MUTATION " + genetic.getMutationProbability() + "f; IA " + behaviour.getName() + ";");
		System.out.println();
		printPopulation(genetic.getPopulation(), genetic.chromossomeSize);

		Tuple<Integer, Integer> progress = new Tuple<>(counter, base_iterations);
		getFacade().sendNotification(GameNotification.SIMULATION_PROGRESS_UPDATE, progress);

		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {
				simulationStart = System.currentTimeMillis();
				genetic.loop();
			}

		});
		t.setDaemon(true);
		t.start();
	}

	private void saveDecks(List<Deck> decks) {
		this.geneticDecks = new ArrayList<>();
		this.allDecks = decks;
	}

	public void setBehaviour(Behaviour behaviour) {
		this.behaviour = behaviour;
	}

	public void setBestResult(Chromosome bestResult) {
		this.bestResult = bestResult;
	}

	public void setDeck1(Deck deck1) {
		this.deck1 = deck1;
	}

	public void setGeneticDecks(List<Deck> geneticDecks) {
		this.geneticDecks = geneticDecks;
		this.genetic.setGeneticDecks(this.geneticDecks);
	}

	public void setIterations(int iterations) {
		this.iterations = iterations;
	}

	//recebeos os dados da UI para algorimo genetico
	private void setParams(int populationSize, int nGames, int iterations, float crossover, float mutation, int timer,
			IBehaviour behaviour, Selection selection, ComboBox<String> deckBox) {

		genetic.saveData(populationSize, nGames, crossover, mutation, selection);
		this.iterations = iterations;
		this.base_iterations = iterations;
		this.behaviour = (Behaviour) behaviour;
		this.counter = 0;
		this.maxConvergenceRetries = timer - 1;

		ObservableList<String> _geneticDecks = deckBox.getItems();

		// we save genetic decks
		for (Deck deck : this.allDecks) {
			for (String _geneticDeck : _geneticDecks) {
				if (_geneticDeck.equals(deck.getName())) {
					this.geneticDecks.add(deck);
					break;
				}
			}
		}

		this.genetic.setGeneticDecks(geneticDecks);

		// we save the selected deck
		String deckSelectedName = deckBox.getSelectionModel().getSelectedItem();

		for (Deck deck : this.geneticDecks) {
			if (deck.getName().equals(deckSelectedName)) {
				setDeck1(deck);
				break;
			}
		}
		
		//bestResult.setFitness(96);
	}
}
