package net.demilich.metastone.game.entities.heroes;

import java.util.ArrayList;

public enum HeroClass {
	ANY,
	DECK_COLLECTION,

	NEUTRAL,

	DRUID,
	HUNTER,
	MAGE,
	PALADIN,
	PRIEST,
	ROGUE,
	SHAMAN,
	WARLOCK,
	WARRIOR,

	SELF,
	OPPONENT,
	BOSS;

	public boolean isBaseClass() {
		HeroClass[] nonBaseClasses = {ANY, NEUTRAL, SELF, DECK_COLLECTION, OPPONENT, BOSS};
		for (int i=0; i<nonBaseClasses.length; i++) {
			if (nonBaseClasses[i] == this) {
				return false;
			}
		}
		return true;
	}
	
	public ArrayList<HeroClass> getHeroes() {
		
		ArrayList<HeroClass> heroes = new ArrayList<>();
		
		heroes.add(HeroClass.DRUID);
		heroes.add(HeroClass.HUNTER);
		heroes.add(HeroClass.MAGE);
		heroes.add(HeroClass.PALADIN);
		heroes.add(HeroClass.PRIEST);
		heroes.add(HeroClass.ROGUE);
		heroes.add(HeroClass.SHAMAN);
		heroes.add(HeroClass.WARLOCK);
		heroes.add(HeroClass.WARRIOR);
		
		return heroes;
	}
}
