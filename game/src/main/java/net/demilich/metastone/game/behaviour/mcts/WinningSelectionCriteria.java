package net.demilich.metastone.game.behaviour.mcts;

/*
 * 4 METODOS para selecionarmos a acao da arvore apos o budget terminar
 * */
public enum WinningSelectionCriteria {
	MaxChild, RobustChild, MaxRobustChild, SecureChild;
}
