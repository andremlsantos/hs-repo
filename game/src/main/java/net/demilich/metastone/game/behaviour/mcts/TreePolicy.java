package net.demilich.metastone.game.behaviour.mcts;

import java.util.Random;
import net.demilich.metastone.game.GameContext;
import net.demilich.metastone.game.actions.ActionType;
import net.demilich.metastone.game.actions.GameAction;

public abstract class TreePolicy {
	// heuristic
	private ProgressiveBias bias;

	// constant
	public static double cp = 1 / Math.sqrt(2);

	public TreePolicy(ProgressiveBias bias) {
		this.bias = bias;
	}

	public double getCp() {
		return cp;
	}

	public void setCp(double cp) {
		this.cp = cp;
	}

	public Node expand(Node parent) {
		// old state from parent node
		GameContext newState = parent.getState().clone();
		int actionIndex = 0;

		if (parent.getAvailableActions().size() > 1) {
			Random randomizer = new Random();
			actionIndex = randomizer.nextInt(parent.getAvailableActions().size());
		}

		// action from previous state GameAction selectedAction =
		GameAction selectedAction = parent.getAvailableActions().get(actionIndex);

		// we remove the selected action
		parent.getAvailableActions().remove(actionIndex);

		// update references
		MctsBehaviour.parentNode = parent;
		MctsBehaviour.parentAction = selectedAction;

		newState.performAction(newState.getActivePlayerId(), selectedAction);

		if (selectedAction.getActionType() == ActionType.END_TURN)
			newState.startTurn(newState.getActivePlayerId());

		// result
		Node node = new Node(newState);
		node.setAction(MctsBehaviour.parentAction); // discover ou nao
		node.setParent(MctsBehaviour.parentNode); // node discover ou nao

		MctsBehaviour.parentNode.getChildren().add(node);

		return node;
	}

	public Node bestChild(Node parent) {
		Node bestNode = null;
		double bestResult = Double.NEGATIVE_INFINITY;

		for (Node candidate : parent.getChildren()) {
			double result = evaluate(candidate, parent, cp) + bias.evaluate(candidate);

			if (result >= bestResult) {
				bestResult = result;
				bestNode = candidate;
			}
		}

		if (bestNode == null)
			System.out.println("erro bestChild(), linha 80, TreePolicy");

		return bestNode;
	}

	// node selection
	public abstract double evaluate(Node child, Node parent, double cp);
}
