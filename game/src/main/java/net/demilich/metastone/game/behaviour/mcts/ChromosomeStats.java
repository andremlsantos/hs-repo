package net.demilich.metastone.game.behaviour.mcts;

import java.util.ArrayList;
import java.util.List;
import net.demilich.metastone.game.decks.Deck;
import net.demilich.metastone.utils.Tuple;

public class ChromosomeStats {

	//propriedades usadas para o calculo do fitnes entre o baralho A vs multiplos
	//uso esta informação como forma de optimizacao, em vez de estar sempre a fazer get()
	int totalVictories = 0;
	String deck1;
	String deck1FileName;
	
	List<Tuple<String, Integer>> stats = new ArrayList<Tuple<String, Integer>>();

	public String getDeck1() {
		return deck1;
	}

	public String getDeck1FileName() {
		return deck1FileName;
	}

	public int getTotalVictories() {
		return totalVictories;
	}

	public List<Tuple<String, Integer>> getValues() {
		return stats;
	}

	public void insertValue(Tuple<String, Integer> stat) {
		totalVictories += stat.getSecond();
		this.stats.add(stat);
	}

	public void setDeck1(String deck1) {
		this.deck1 = deck1;
	}

	public void setDeck1FileName(String deck1FileName) {
		this.deck1FileName = deck1FileName;
	}

	public void setTotalVictories(int totalVictories) {
		this.totalVictories = totalVictories;
	}

	public void setValues(List<Tuple<String, Integer>> results) {
		this.stats = results;
	}
}
