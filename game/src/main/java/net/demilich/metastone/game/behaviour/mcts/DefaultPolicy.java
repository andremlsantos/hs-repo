package net.demilich.metastone.game.behaviour.mcts;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import net.demilich.metastone.game.GameContext;
import net.demilich.metastone.game.Player;
import net.demilich.metastone.game.actions.ActionType;
import net.demilich.metastone.game.actions.GameAction;
import net.demilich.metastone.game.behaviour.Behaviour;
import net.demilich.metastone.utils.Tuple;

public abstract class DefaultPolicy {
	
	// selection of random K actions
	protected int mctsK; // min value

	protected Behaviour mctsPolicy = null; 
	protected int opponentK; // max value
	protected Behaviour opponentPolicy = null;

	protected IHeuristic heuristic;
	protected boolean isSampling;

	protected DecksSampling decksSampling;	//referencia a amostragem
	protected int[] percentage;				//diferentes valores de k
	protected boolean isDinamic;			//se usamos k em percentagem (dinamico) ou k fixo == valor

	//tempo maximo que uma simulacao pode semorar em segundos
	private int maxTime = 2;				

	//nodes statistics
	private int victories, visits;

	public DefaultPolicy() {}
	
	public DefaultPolicy(IHeuristic heuristic, boolean isDynamic, int kPlayer, int kOpponent, int[] percentage) {
		this.heuristic = heuristic;
		this.mctsK = kPlayer;
		this.opponentK = kOpponent;
		this.percentage = percentage;
		this.isDinamic = isDynamic;
	}

	//our way of inserting heuristic knowledge into this policy
	public static List<GameAction> tounementSelection(List<GameAction> actions, int times) {
		if (times >= actions.size())
			return actions;

		List<Integer> actionIndexes = new ArrayList<>();
		List<GameAction> result = new ArrayList<>();
		int indexesSize = actions.size();

		for (int i = 0; i < indexesSize; i++) {
			actionIndexes.add(i);
		}

		Random randomizer = new Random();

		for (int i = 0; i < times; i++) {
			int randomIndex = randomizer.nextInt(indexesSize);
			int actionIndex = actionIndexes.get(randomIndex);
			result.add(actions.get(actionIndex));
			actionIndexes.remove(randomIndex);
			indexesSize--;
		}

		return result;
	}

	//since the k value is a percentage
	//we convert the percentage in a integer, according the available's number of actions
	//50% of 50 actions is 25
	//we have a minimum number of actions to evaluate
	//if we choose 0%, we automatically evaluate minActions == 1 
	public static int setupK(List<GameAction> validActions, boolean isDynamic, int minActions, int percentage) {
		int actionsCount = validActions.size();
		int result = 0;

		if (isDynamic) {
			int percentageK = Math.round((actionsCount * percentage) / 100);

			if (percentageK > minActions) {
				result = percentageK;
			} else {
				result = Math.min(actionsCount, minActions);
			}
		} else {
			result = Math.min(actionsCount, minActions);
		}

		return result;
	}

	public static Future<?> simulation;
	
	//FAZ TODOS OS ROLL-OUTS 
	//devido � natureza estocastica do algoritmo, por vezes occore problemas nas simula�oes
	//cada simula�ao individual � enviada pra uma thread
	//se houver algum problema -> n�s paramos a execu��o da thread
	//solu��o generica, que funciona sempre
	//sempre que uma simula��o demora muito tempo, n�s tbm paramos essa mesma simula��o
	public Tuple<Integer, Integer> rollOut(GameContext state, int times) {
		this.victories = 0; 
		this.visits = 0;
		ExecutorService executor = null;

		//sampling + default police AI
		GameContext game = setupRollOut(state.clone());
		
		for (int i = 0; i < times; i++) {
			executor = Executors.newFixedThreadPool(6);
			DefaultPolicy.simulation = executor.submit(new Runnable() {
				@Override
				public void run() {
					executeRollOut(game.clone());
				}
			});

			try {
				simulation.get(this.maxTime, TimeUnit.SECONDS);
			} catch (Exception e) {
				System.out.println("Max time ended");
			}
			executor.shutdown();
		}

		return new Tuple<Integer, Integer>(victories, visits);
	}
	
	//we choose the actions, until a leaf node has been reached
	private void executeRollOut(GameContext game) {
		while (!game.gameDecided()) {

			GameAction actionReturned = null;
			List<GameAction> validActions = game.getValidActions();

			if (game.getActivePlayerId() == MctsBehaviour.playerID) {
				actionReturned = mctsPolicy.requestAction(game, game.getActivePlayer(), validActions);
				game.performAction(game.getActivePlayerId(), actionReturned);
			} else {
				actionReturned = opponentPolicy.requestAction(game, game.getActivePlayer(), validActions);
				game.performAction(game.getActivePlayerId(), actionReturned);
			}

			// end turn state
			if (actionReturned.getActionType() == ActionType.END_TURN)
				game.startTurn(game.getActivePlayerId());
		}

		this.victories += game.getWinningPlayerId() == MctsBehaviour.playerID ? 1 : 0;
		this.visits += 1;
		game.dispose();
	}

	/*
	 * we setup: 
	 * AI during simulations, 
	 * sample deck used
	 */
	public GameContext setupRollOut(GameContext state) {
		// player 1-MCTS
		// player 2-other
		if (state.getPlayer1().getId() == MctsBehaviour.playerID) {
			setupBehavior(state.getPlayer1(), state.getPlayer2());
			
			if (isSampling) {
				Tuple<Player, Player> samplePlayers = setupSampling(state, state.getPlayer1(), state.getPlayer2());

				state.setPlayerById(state.getPlayer1().getId(), samplePlayers.getFirst());
				state.setPlayerById(state.getPlayer2().getId(), samplePlayers.getSecond());
			}

			state.getPlayer1().setBehaviour(mctsPolicy);
			state.getPlayer2().setBehaviour(opponentPolicy);
		}
		// player 1-other
		// player 2-MCTS
		else {
			setupBehavior(state.getPlayer2(), state.getPlayer1());
			
			if (isSampling) {
				Tuple<Player, Player> samplePlayers = setupSampling(state, state.getPlayer2(), state.getPlayer1());

				state.setPlayerById(state.getPlayer1().getId(), samplePlayers.getSecond());
				state.setPlayerById(state.getPlayer2().getId(), samplePlayers.getFirst());
			}

			state.getPlayer1().setBehaviour(opponentPolicy);
			state.getPlayer2().setBehaviour(mctsPolicy);
		}
		return state;
	}

	//ABSTRACT BEHAVIOUR
	
	// do we need sampling? then execute it
	// otherwise just clone the state and both players
	protected abstract Tuple<Player, Player> setupSampling(GameContext state, Player mcts, Player opponent);
	
	// what AI we want to execute in default police
	protected abstract void setupBehavior(Player player, Player opponent);
	
	//GENERAL STUFF
	public int getMctsK() {
		return mctsK;
	}

	public int getOpponentK() {
		return opponentK;
	}

	public int[] getPercentage() {
		return percentage;
	}

	public boolean isDinamic() {
		return isDinamic;
	}

	public void setDinamic(boolean isDinamic) {
		this.isDinamic = isDinamic;
	}

	public void setMctsK(int mctsK) {
		this.mctsK = mctsK;
	}

	public void setOpponentK(int opponentK) {
		this.opponentK = opponentK;
	}

	public void setPercentage(int[] percentage) {
		this.percentage = percentage;
	}
}