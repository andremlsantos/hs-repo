package net.demilich.metastone.gui.mctsmode;

import java.util.ArrayList;
import java.util.List;

import net.demilich.metastone.GameNotification;
import net.demilich.metastone.game.decks.Deck;
import net.demilich.metastone.game.decks.DeckFormat;
import net.demilich.metastone.game.gameconfig.GameConfig;
import net.demilich.metastone.gui.playmode.PlayModeMediator;
import net.demilich.nittygrittymvc.Mediator;
import net.demilich.nittygrittymvc.interfaces.INotification;

//padrao desenho mcts e simulador
public class MctsMediator extends Mediator<GameNotification>{

	public static final String NAME = "MCTSMediator";
	
	//VIEW
	private final MctsModeConfigView view;
	
	public MctsMediator() {
		super(NAME);		
		view = new MctsModeConfigView();
	}

	@Override
	public void handleNotification(INotification<GameNotification> notification) {
		switch (notification.getId()) {
		case REPLY_DECKS:
			@SuppressWarnings("unchecked")
			List<Deck> decks = (List<Deck>) notification.getBody();
			view.initView(decks);
			break;
		case REPLY_DECK_FORMATS:
			//@SuppressWarnings("unchecked")
			//List<DeckFormat> deckFormats = (List<DeckFormat>) notification.getBody();
			//view.injectDecks(deckFormats);
			break;
		default:
			break;
		}
	}
	
	@Override
	public List<GameNotification> listNotificationInterests() {
		List<GameNotification> notificationInterests = new ArrayList<GameNotification>();
		notificationInterests.add(GameNotification.REPLY_DECKS);
		notificationInterests.add(GameNotification.REPLY_DECK_FORMATS);
		return notificationInterests;
	}
	
	@Override
	public void onRegister() {
		//pedir os decks
		getFacade().sendNotification(GameNotification.REQUEST_DECKS);
		
		//mostrar a view
		getFacade().sendNotification(GameNotification.SHOW_VIEW, view);
	}
}
