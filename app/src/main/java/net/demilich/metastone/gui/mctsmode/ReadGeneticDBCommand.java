package net.demilich.metastone.gui.mctsmode;

import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import net.demilich.metastone.game.behaviour.mcts.Genetic;

//leitura dos pesos do algoritmo genetico para heuritica 1
//lemos de json
public class ReadGeneticDBCommand extends Command {

	private String fileName;

	public void setDeckName(String fileName) {
		this.fileName = fileName;
	}
	
	@Override
	public void setData(Object[] data) {
		String fileName = (String) data[0];
		this.fileName = fileName;
	}

	@Override
	public void execute() {
		readDeckWeights(this.fileName);
	}

	@SuppressWarnings("unchecked")
	private static void readDeckWeights(String fileName) {
		JSONParser parser = new JSONParser();

		try {
			Object obj = parser.parse(new FileReader(GeneticDB.Path + "\\" + fileName+".json"));
			JSONObject jsonObject = (JSONObject) obj;

			// weights
			int parameters = Genetic.chromossomeSize;
			double[] weights = new double[parameters];

			JSONArray values = (JSONArray) jsonObject.get("values");
			Iterator<Double> iterator = values.iterator();
			int index = 0;

			while (iterator.hasNext()) {
				double value = iterator.next().doubleValue();
				weights[index++] = value;
			}

			GeneticDB.getInstance().setValues(weights);

		} catch (Exception e) {
			System.out.println("NO weigths found!");
			createFolder(GeneticDB.Path);
		}
	}
}
