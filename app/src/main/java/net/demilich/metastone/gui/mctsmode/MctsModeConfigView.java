package net.demilich.metastone.gui.mctsmode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.VBox;
import net.demilich.metastone.GameNotification;
import net.demilich.metastone.NotificationProxy;
import net.demilich.metastone.game.behaviour.mcts.Selection;
import net.demilich.metastone.game.behaviour.mcts.WinningSelectionCriteria;
import net.demilich.metastone.game.decks.Deck;
import net.demilich.metastone.game.entities.heroes.HeroClass;

//UI do ecra de configuracao do mcts
public class MctsModeConfigView extends ScrollPane implements EventHandler<ActionEvent> {

	@FXML
	private Button backButton;

	@FXML
	private Button saveButton;

	@FXML
	private VBox vboxWarrior, vboxPaladin, vboxDruid, vboxRogue, vboxWarlock, vboxHunter, vboxShaman, vboxMage,
			vboxPriest;
	private ArrayList<VBox> heroVBox;
	private ArrayList<HeroBox> heroBox;

	@FXML
	private ChoiceBox<String> difficultyBox;
	
	@FXML 
	private ChoiceBox<WinningSelectionCriteria> selection;

	@FXML
	private TextField beginnerIterations, mediumIterations, hardIterations, beginnerRollOut, mediumRollOut, hardRollOut,
			kPlayer, kOpponent;

	@FXML
	private Slider slider, slider1;

	@FXML
	CheckBox iterating, actions;

	public MctsModeConfigView() {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/MctsConfigView.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();

		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}

		backButton.setOnAction(this);
		saveButton.setOnAction(this);
	}

	@Override
	public void handle(ActionEvent actionEvent) {
		if (actionEvent.getSource() == backButton) {
			NotificationProxy.sendNotification(GameNotification.MAIN_MENU);
		} else if (actionEvent.getSource() == saveButton) {
			DecksDB.getInstance().writeDecks(heroBox);

			boolean isValidInput = MctsSetup.getInstance().writeConfigs(difficultyBox, beginnerIterations,
					mediumIterations, hardIterations, beginnerRollOut, mediumRollOut, hardRollOut, kPlayer, kOpponent,
					slider, slider1, iterating, actions, selection);

			if (isValidInput)
				NotificationProxy.sendNotification(GameNotification.MAIN_MENU);
		}
	}

	public void initView(List<Deck> decks) {
		// GRAB DATA to set view
		DecksDB.getInstance().setBuilderDecks(decks); // from deck builder
		DecksDB.getInstance().readDecks(); // from json

		MctsSetup.getInstance().readConfigs(); // n iterations, roll-outs...

		setupView();
	}

	private void setupView() {
		// heroes label
		setupHeroLabel();

		// heroes decks
		setupHeroDecks();

		// setup mcts iterations
		setupConfigs();
	}

	private void setupConfigs() {
		difficultyBox.setItems(FXCollections.observableArrayList("Easy", "Normal", "Hard"));
		difficultyBox.setTooltip(new Tooltip("Select the  MCTS-Difficulty"));

		ObservableList<WinningSelectionCriteria> selectionList = FXCollections.observableArrayList();
		selectionList.add(WinningSelectionCriteria.MaxChild);
		selectionList.add(WinningSelectionCriteria.MaxRobustChild);
		selectionList.add(WinningSelectionCriteria.RobustChild);
		selectionList.add(WinningSelectionCriteria.SecureChild);

		selection.setItems(selectionList);
		selection.setTooltip(new Tooltip("Select the winning selection mcts criteria"));
		
		beginnerIterations.setTooltip(new Tooltip("Insert the number of MCTS-EASY iterations"));
		mediumIterations.setTooltip(new Tooltip("Insert the number of MCTS-Normal iterations"));
		hardIterations.setTooltip(new Tooltip("Insert the number of MCTS-Hard iterations"));

		beginnerRollOut.setTooltip(new Tooltip("Insert the number of MCTS-EASY roll-out"));
		mediumRollOut.setTooltip(new Tooltip("Insert the number of MCTS-Normal roll-out"));
		hardRollOut.setTooltip(new Tooltip("Insert the number of MCTS-Hard roll-out"));

		kPlayer.setTooltip(new Tooltip("Insert the number of k actions to be used in default policy"));
		kOpponent.setTooltip(new Tooltip("Insert the number of k actions to be used in default policy"));

		try {
			difficultyBox.getSelectionModel().select(MctsSetup.getInstance().getDificulty());
			selection.getSelectionModel().select(MctsSetup.getInstance().getSelectionCriteria());

			beginnerIterations.setText(Integer.toString(MctsSetup.getInstance().getIterations()[0]).toString());
			mediumIterations.setText(Integer.toString(MctsSetup.getInstance().getIterations()[1]).toString());
			hardIterations.setText(Integer.toString(MctsSetup.getInstance().getIterations()[2]).toString());

			beginnerRollOut.setText(Integer.toString(MctsSetup.getInstance().getRollOut()[0]).toString());
			mediumRollOut.setText(Integer.toString(MctsSetup.getInstance().getRollOut()[1]).toString());
			hardRollOut.setText(Integer.toString(MctsSetup.getInstance().getRollOut()[2]).toString());

			kPlayer.setText(Integer.toString(MctsSetup.getInstance().getkActions()[0]).toString());
			kOpponent.setText(Integer.toString(MctsSetup.getInstance().getkActions()[1]).toString());
			slider.setValue(MctsSetup.getInstance().getkActions()[3]);
			slider1.setValue(MctsSetup.getInstance().getkActions()[4]);
			iterating.setSelected(MctsSetup.getInstance().getIterations()[3] == 1?true:false);
			actions.setSelected(MctsSetup.getInstance().getkActions()[2] == 1?true:false);

		} catch (Exception e) {
			difficultyBox.getSelectionModel().select(0);
			selection.getSelectionModel().select(0);
			
			beginnerIterations.setText("Insert the number of MCTS-EASY iterations");
			mediumIterations.setText("Insert the number of MCTS-Normal iterations");
			hardIterations.setText("Insert the number of MCTS-Hard iterations");

			beginnerRollOut.setText("Insert the number of MCTS-EASY roll-out");
			mediumRollOut.setText("Insert the number of MCTS-Normal roll-out");
			hardRollOut.setText("Insert the number of MCTS-Hard roll-out");

			kPlayer.setText("Insert the number of k actions to be used in default policy");
			kOpponent.setText("Insert the number of k actions to be used in default policy");
		}
	}

	private void setupHeroLabel() {
		heroVBox = new ArrayList<>();
		heroBox = new ArrayList<>();

		heroVBox.add(vboxDruid);
		heroVBox.add(vboxHunter);
		heroVBox.add(vboxMage);
		heroVBox.add(vboxPaladin);
		heroVBox.add(vboxPriest);
		heroVBox.add(vboxRogue);
		heroVBox.add(vboxShaman);
		heroVBox.add(vboxWarlock);
		heroVBox.add(vboxWarrior);

		ArrayList<HeroClass> heroes = HeroClass.ANY.getHeroes();

		for (int i = 0; i < heroes.size(); i++) {

			HeroClass hero = heroes.get(i);

			Label heroLabel = new Label(hero.name());
			heroLabel.setAlignment(Pos.CENTER);
			heroLabel.setMaxWidth(Double.MAX_VALUE);
			heroLabel.setPadding(new Insets(5, 5, 5, 5));

			HeroBox box = new HeroBox();
			box.setHeroName(heroLabel);

			heroBox.add(box);
			heroVBox.get(i).getChildren().add(box.getHeroName());
		}
	}

	private void setupHeroDecks() {
		ArrayList<HeroClass> heroes = HeroClass.ANY.getHeroes();

		for (int i = 0; i < heroes.size(); i++) {
			setupHeroDeck(heroes.get(i), i);
		}
	}

	private void setupHeroDeck(HeroClass hero, int indexVBOX) {
		// all decks
		ArrayList<Deck> builderDecks = DecksDB.getInstance().getHeroDecks(hero,
				DecksDB.getInstance().getBuilderDecks());
		ArrayList<Deck> jsonDecks = DecksDB.getInstance().getHeroDecks(hero, DecksDB.getInstance().getJsonDecks());
		ArrayList<CheckBox> checkBoxes = new ArrayList<>();

		for (Deck deck : builderDecks) {
			CheckBox check = new CheckBox(deck.getName());

			if (DecksDB.getInstance().containsDeck(hero, deck))
				check.setSelected(true);

			checkBoxes.add(check);
		}

		heroBox.get(indexVBOX).setDecks(checkBoxes);
		heroVBox.get(indexVBOX).getChildren().addAll(checkBoxes);
	}

	@FXML
	private void cleanText() {
		// TODO
	}
}
