package net.demilich.metastone.gui.mctsmode;

import java.util.ArrayList;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;

public class HeroBox {

	private Label heroName;
	private ArrayList<CheckBox> decks;

	public Label getHeroName() {
		return heroName;
	}

	public void setHeroName(Label heroName) {
		this.heroName = heroName;
	}

	public ArrayList<CheckBox> getDecks() {
		return decks;
	}

	public void setDecks(ArrayList<CheckBox> decks) {
		this.decks = decks;
	}
}
