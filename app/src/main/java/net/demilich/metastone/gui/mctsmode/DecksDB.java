package net.demilich.metastone.gui.mctsmode;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import net.demilich.metastone.game.decks.Deck;
import net.demilich.metastone.game.entities.heroes.HeroClass;
import net.demilich.metastone.gui.deckbuilder.DeckProxy;
import net.demilich.metastone.utils.UserHomeMetastone;

/*
 * ler e escrever baralhos selecionados da base dados de baralhos
 * para o ficheiro json
 * */
public class DecksDB {

	//singleton
	private static DecksDB instance = null;

	//VARIABLES
	public static String fileName = "deckDB.json";
	public static final  String DB_FOLDER = "db";
	public static final String Path = UserHomeMetastone.getPath() + File.separator + DB_FOLDER;

	//read & write
	private Command reader = new ReadDBCommand();
	private Command writer = new WriteDBCommand();
	
	//KNOWLEDGE
	private static List<Deck> builderDecks;
	private static List<Deck> jsonDecks;

	public static DecksDB getInstance() {
		if (instance == null) {
			instance = new DecksDB();
		}

		return instance;
	}

	public void setBuilderDecks(List<Deck> decks) {
		DecksDB.builderDecks = decks;
	}

	public List<Deck> getBuilderDecks() {
		return this.builderDecks;
	}


	public static List<Deck> getJsonDecks() {
		return jsonDecks;
	}

	public static void setJsonDecks(List<Deck> jsonDecks) {
		DecksDB.jsonDecks = jsonDecks;
	}

	// Return decks by Hero
	static ArrayList<Deck> getHeroDecks(HeroClass hero, List<Deck> decks) {
		ArrayList<Deck> heroDecks = new ArrayList<>();

		for (Deck deck : decks) {
			if (deck.getHeroClass().equals(hero)) {
				heroDecks.add(deck);
			}
		}

		return heroDecks;
	}

	//id some hero has deck name
	static Deck containsDeckName(HeroClass hero, String deckName) {	
		ArrayList<Deck> heroDecks = getHeroDecks(hero, builderDecks);
		
		for (Deck deck : heroDecks) {
			if(deck.getName().equals(deckName))
				return deck;
		}
		
		return null;
	}
	
	//if some specific deck exists in json
	public boolean containsDeck(HeroClass hero, Deck builder) {
		for (Deck deck : jsonDecks) {
			if(deck.getName().equals(builder.getName()))
				return true;			
		}
		
		return false;
	}
	
	//write from file
	public void writeDecks(ArrayList<HeroBox> heroBox) {
		//we set view frist, to be able to get the selected decks!
		Object[] data = {heroBox};
		writer.setData(data);
		try {
			writer.execute();
		} catch (Exception e) {
			System.out.println("Error writing decks, 109, DecksDB");
		}
	}

	//read from file
	public void readDecks() {		
		try {
			reader.execute();
		} catch (Exception e) {
			System.out.println("Error reading decks, 118, DecksDB");
		}
	}
}
