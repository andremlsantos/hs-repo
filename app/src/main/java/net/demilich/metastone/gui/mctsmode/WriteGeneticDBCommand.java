package net.demilich.metastone.gui.mctsmode;

import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import net.demilich.metastone.game.behaviour.mcts.ChromosomeStats;
import net.demilich.metastone.utils.Tuple;

//guardamos as configuracoes do algoritmo genetico
//para json, usamos a biblioteca simple json
public class WriteGeneticDBCommand extends Command {

	private int populationSize, games, iterations, fitness, rank;
	private float crossoverProb, mutationProb;
	private String selection, IA;
	private double[] values, fitnessRatio;
	private ChromosomeStats stats;

	@Override
	public void setData(Object[] data) {
		this.populationSize = (int) data[0];
		this.games = (int) data[1];
		this.iterations = (int) data[2];
		this.fitness = (int) data[7];
		this.rank = (int) data[9];

		this.values = (double[]) data[6];

		this.selection = (String) data[5];

		this.crossoverProb = (float) data[3];
		this.mutationProb = (float) data[4];
		this.fitnessRatio = (double[]) data[8];

		this.stats = (ChromosomeStats) data[10];
		this.IA = (String) data[11];
	}

	@SuppressWarnings("unchecked")
	@Override
	public void execute() {
		JSONObject json = new JSONObject();

		json.put("populationSize", new Integer(populationSize));
		json.put("games", new Integer(games));
		json.put("iterations", new Integer(iterations));
		json.put("fitness", new Integer(fitness));
		json.put("rank", new Integer(rank));

		json.put("crossoverProb", new Float(crossoverProb));
		json.put("mutationProb", new Float(mutationProb));

		json.put("selection", new String(selection));

		JSONArray _values = new JSONArray();

		for (double weight : this.values) {
			_values.add(weight);
		}
		json.put("values", _values);

		JSONArray fitnessRatio = new JSONArray();
		fitnessRatio.add(new Double(this.fitnessRatio[0]));
		fitnessRatio.add(new Double(this.fitnessRatio[1]));
		json.put("fitnessRatio", fitnessRatio);

		// stats
		JSONArray stats = new JSONArray();
		for (Tuple<String, Integer> stat : this.stats.getValues()) {
			stats.add(stat.getFirst() + ": " + stat.getSecond());
		}
		json.put("stats", stats);

		json.put("IA", new String(this.IA));

		String fileName = this.stats.getDeck1() + ".json";
		try {
			writeToFile(GeneticDB.Path, fileName, json);
			//writeJson(fileName, json);
		} catch (IOException e) {
			System.out.println("Error saving MCTS configs");
		}
	}
}
