package net.demilich.metastone.gui.mctsmode;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import net.demilich.metastone.GameNotification;
import net.demilich.metastone.NotificationProxy;
import net.demilich.metastone.game.behaviour.GreedyOptimizeMove;
import net.demilich.metastone.game.behaviour.IBehaviour;
import net.demilich.metastone.game.behaviour.NoAggressionBehaviour;
import net.demilich.metastone.game.behaviour.PlayRandomBehaviour;
import net.demilich.metastone.game.behaviour.heuristic.WeightedHeuristic;
import net.demilich.metastone.game.behaviour.human.HumanBehaviour;
import net.demilich.metastone.game.behaviour.mcts.Chromosome;
import net.demilich.metastone.game.behaviour.mcts.MctsBehaviour;
import net.demilich.metastone.game.behaviour.mcts.Selection;
import net.demilich.metastone.game.behaviour.threat.GameStateValueBehaviour;
import net.demilich.metastone.gui.common.BehaviourStringConverter;
import net.demilich.metastone.gui.playmode.config.PlayerConfigType;

//UI
public class GeneticTradeView extends VBox implements EventHandler<ActionEvent> {

	@FXML
	private Button backButton, run;

	@FXML
	private TextField population, fitness, iteartions, crossover, mutation, retries;

	@FXML
	private Text heuristic;

	@FXML
	private ComboBox<IBehaviour> behaviourBox;

	@FXML
	private ComboBox<Selection> selectionBox;

	@FXML
	private ComboBox<String> deckBox;

	public GeneticTradeView() {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/GeneticTradeView.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();

		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
		backButton.setOnAction(this);
		run.setOnAction(this);

		setupConfigs();
	}

	private void setupConfigs() {
		population.setText("30");
		fitness.setText("30");
		iteartions.setText("10");
		crossover.setText("0.7");
		mutation.setText("0.01");
		retries.setText("4");

		population.setTooltip(new Tooltip("Insert the number of MCTS-EASY iterations"));
		fitness.setTooltip(new Tooltip("Insert the number of MCTS-EASY iterations"));
		iteartions.setTooltip(new Tooltip("Insert the number of MCTS-EASY iterations"));
		crossover.setTooltip(new Tooltip("Insert the number of MCTS-EASY iterations"));
		mutation.setTooltip(new Tooltip("Insert the number of MCTS-EASY iterations"));
		retries.setTooltip(new Tooltip("Insert the number of MCTS-EASY iterations"));

		ObservableList<IBehaviour> behaviourList = FXCollections.observableArrayList();
		behaviourList.add(new GameStateValueBehaviour());
		behaviourList.add(new PlayRandomBehaviour());
		behaviourList.add(new GreedyOptimizeMove(new WeightedHeuristic()));

		behaviourBox.setItems(behaviourList);
		behaviourBox.getSelectionModel().selectFirst();
		behaviourBox.setConverter(new BehaviourStringConverter());

		ObservableList<Selection> selectionList = FXCollections.observableArrayList();
		selectionList.add(Selection.RANDOM);
		selectionList.add(Selection.RANK);
		selectionList.add(Selection.ROULETTE);
		selectionList.add(Selection.TOURNAMENT);

		selectionBox.setItems(selectionList);
		selectionBox.getSelectionModel().selectFirst();

		ArrayList<String> decks = GeneticDB.getInstance().getGeneticDecksNames();
		ObservableList<String> geneticDeckList = FXCollections.observableArrayList();

		for (String deck : decks) {
			geneticDeckList.add(deck);
		}

		deckBox.setItems(geneticDeckList);
		deckBox.getSelectionModel().selectFirst();
	}

	@Override
	public void handle(ActionEvent actionEvent) {
		if (actionEvent.getSource() == backButton) {
			NotificationProxy.sendNotification(GameNotification.MAIN_MENU);
		} else if (actionEvent.getSource() == run) {
			int resultPopulation = (int) MctsSetup.isValid(population, Integer.class);
			int resultFitness = (int) MctsSetup.isValid(fitness, Integer.class)
					* GeneticDB.getInstance().getGeneticDecksNames().size();
			int resultiterations = (int) MctsSetup.isValid(iteartions, Integer.class);
			float resultCrossover = MctsSetup.isValid(crossover, Float.class);
			float resultMutation = MctsSetup.isValid(mutation, Float.class);
			int resultTimer = (int) MctsSetup.isValid(retries, Integer.class);

			IBehaviour behaviour = behaviourBox.getSelectionModel().getSelectedItem();
			Selection selection = selectionBox.getSelectionModel().getSelectedItem();

			if (resultPopulation > 0 && (resultPopulation / 2) * 2 == resultPopulation && resultFitness > 0
					&& resultiterations > 0 && resultCrossover > 0 && resultMutation > 0 && resultTimer > 0) {
				Object[] configs = { population.getText(), resultFitness + "", iteartions.getText(),
						crossover.getText(), mutation.getText(), retries.getText(), behaviour, selection, deckBox };
				NotificationProxy.sendNotification(GameNotification.COMMIT_SIMULATIONMODE_CONFIG, configs);
			}
		}
	}

	@FXML
	private void cleanText() {
	}

	public void save(Object body) {
		GeneticDB.getInstance().writeGeneticDeck((Object[]) body);
	}

	@SuppressWarnings("unused")
	public void print(Object body) {
		Chromosome result = (Chromosome) body;
		DecimalFormat decimalFormat = new DecimalFormat("#.##");
		String heuristicText = "MinionAdvantage(" + Float.valueOf(decimalFormat.format(result.getValues()[0]) + "") + ") + "
				+ "ToughMinionAdvantage(" + Float.valueOf(decimalFormat.format(result.getValues()[1]) + "") + ") + "
				+ "HandAdvantage(" + Float.valueOf(decimalFormat.format(result.getValues()[2]) + "") + ") + "
				+ "TradeAdvantage(" + Float.valueOf(decimalFormat.format(result.getValues()[3]) + "") + ") + "
				+ "BoardManaAdvantage(" + Float.valueOf(decimalFormat.format(result.getValues()[4]) + "") + ")";

		heuristic.setText(heuristicText);
	}

}
