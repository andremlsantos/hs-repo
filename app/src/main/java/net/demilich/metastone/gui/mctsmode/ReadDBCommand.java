package net.demilich.metastone.gui.mctsmode;

import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import net.demilich.metastone.game.decks.Deck;
import net.demilich.metastone.game.entities.heroes.HeroClass;
import net.demilich.metastone.gui.deckbuilder.DeckProxy;

//leitura dos baralhos selecionados na UI
public class ReadDBCommand extends Command {
	
	@Override
	public void execute() {		
		readJSONDecks();
	}

	private void readJSONDecks() {
		ArrayList<Deck> result = new ArrayList<>();

		for (HeroClass hero : HeroClass.ANY.getHeroes()) {

			JSONArray decks = readHeroDeck(hero);

			if (decks != null) {
				Iterator file = decks.iterator();

				while (file.hasNext()) {

					String deckName = file.next().toString();

					Deck deck = DecksDB.containsDeckName(hero, deckName);

					if (deck != null)
						result.add(deck);
				}
			}
		}
		DecksDB.getInstance().setJsonDecks(result);
	}
	
	private static JSONArray readHeroDeck(HeroClass hero) {
		JSONParser parser = new JSONParser();
		JSONArray heroDeck = null;

		try {
			Object obj = parser.parse(new FileReader(DecksDB.Path + "\\" + DecksDB.fileName));
			JSONObject jsonObject = (JSONObject) obj;

			// DECKS
			heroDeck = (JSONArray) jsonObject.get(hero.name());

		} catch (Exception e) {
			createFolder(DecksDB.Path);
		}

		return heroDeck;
	}

	@Override
	public void setData(Object[] data) { }
}
