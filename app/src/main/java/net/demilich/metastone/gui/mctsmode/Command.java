package net.demilich.metastone.gui.mctsmode;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;

import org.json.simple.JSONObject;

/*
 * leitura e escrita do json
 * */
public abstract class Command {
	public abstract void execute() throws IOException, URISyntaxException;
	
	public abstract void setData(Object[] data);
	
	protected static boolean createFolder(String path) {
		File folder = new File(path);
		
		if(!folder.exists()) {
		    try {
		    	folder.mkdir();
		        return true;
		    } 
		    catch (Exception e) {	
		    	return false;
		    }
		}
		return false;		
	} 
	
	protected static void writeToFile(String path, String fileName, JSONObject json) throws IOException {
		try (FileWriter file = new FileWriter(path + "\\" + fileName)) {
			// file = new FileWriter(Path + filename);
			file.write(json.toJSONString());
			file.flush();
			file.close();
		} catch (IOException e) {
			boolean isSuccces = createFolder(path);

			if (isSuccces) {
				FileWriter file = new FileWriter(path + "\\" + fileName);
				file.write(json.toJSONString());
				file.flush();
				file.close();
			}
		}
	}
}
