package net.demilich.metastone.gui.mctsmode;

import java.io.File;

import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.paint.Color;
import net.demilich.metastone.game.behaviour.mcts.WinningSelectionCriteria;
import net.demilich.metastone.utils.UserHomeMetastone;

//faz a validacao dos parametros introduzidos no ecra de configuracao de mcts
public class MctsSetup {

	// singleton
	private static MctsSetup instance = null;

	// VARIABLES
	public static String fileName = "config.json";
	public static final String CONFIG_FOLDER = "config";
	public static final String Path = UserHomeMetastone.getPath() + File.separator + CONFIG_FOLDER;

	// DATA
	private static int[] iterations;
	private static int[] rollOut;

	private static int[] kActions;
	private static int dificulty;
	private static int selectionCriteria;

	public static MctsSetup getInstance() {
		if (instance == null) {
			instance = new MctsSetup();
		}

		return instance;
	}

	public static int[] getkActions() {
		return kActions;
	}

	public static int[] getRollOut() {
		return rollOut;
	}

	public static void setkActions(Long[] kActions) {
		int[] clone = new int[] { Math.toIntExact(kActions[0]), Math.toIntExact(kActions[1]), Math.toIntExact(kActions[2]), Math.toIntExact(kActions[3]), Math.toIntExact(kActions[4]) };

		MctsSetup.kActions = clone;
	}

	public static void setRollOut(Long[] rollOut) {
		int[] clone = new int[] { Math.toIntExact(rollOut[0]), Math.toIntExact(rollOut[1]), Math.toIntExact(rollOut[2]) };
		
		MctsSetup.rollOut = clone;
	}

	private ReadConfigCommand reader = new ReadConfigCommand();

	private WriteConfigCommand writer = new WriteConfigCommand();

	public int getDificulty() {
		return this.dificulty;
	}

	public int[] getIterations() {
		return this.iterations;
	}

	// validate input
	public static float isValid(TextField input, Class<?> cls) {
		float result = -1;

		try {
			if(cls == Integer.class)
				result = Integer.parseInt(input.getText());
			else 
				result = Float.parseFloat(input.getText());

			if (result <= 0) {
				input.getStyleClass().remove("text-field");
				input.getStyleClass().add("error-text-field");
			} else {
				input.getStyleClass().remove("error-text-field");
				input.getStyleClass().add("text-field");
			}

		} catch (Exception e) {
			input.setText("");
			input.getStyleClass().remove("text-field");
			input.getStyleClass().add("error-text-field");
		}
		return result;
	}

	public void readConfigs() {
		reader.execute();
	}

	public void setDifficulty(Long dificulty) {
		MctsSetup.dificulty = Math.toIntExact(dificulty);
	}

	public void setIterations(Long[] iterations) {
		int[] clone = new int[] { Math.toIntExact(iterations[0]), Math.toIntExact(iterations[1]), Math.toIntExact(iterations[2]), Math.toIntExact(iterations[3]) };
		
		MctsSetup.iterations = clone;
	}

	public boolean writeConfigs(ChoiceBox<String> difficultyBox, TextField beginnerIterations,
			TextField mediumIterations, TextField hardIterations, TextField beginnerRollOut, TextField mediumRollOut,
			TextField hardRollOut, TextField kPlayer, TextField kOpponent,
			Slider playerSlider, Slider opponentSlider, CheckBox iterating, CheckBox actions, ChoiceBox<WinningSelectionCriteria> selectionCriteria) {

		// VALIDATE DATA
		int difficultySelected = difficultyBox.getSelectionModel().getSelectedIndex();
		int criteria = selectionCriteria.getSelectionModel().getSelectedIndex();

		int resultBeginnerIterations = (int) isValid(beginnerIterations, Integer.class);
		int resultMediumIterations = (int) isValid(mediumIterations, Integer.class);
		int resultHardIterations = (int) isValid(hardIterations, Integer.class);

		int resultBeginnerRollOut = (int) isValid(beginnerRollOut, Integer.class);
		int resultMediumRollOut = (int) isValid(mediumRollOut, Integer.class);
		int resultHardRollOut = (int) isValid(hardRollOut, Integer.class);

		int resultPlayer = (int) isValid(kPlayer, Integer.class);
		int resultOpponent = (int) isValid(kOpponent, Integer.class);
		
		int resultSliderPlayer = (int)playerSlider.getValue();
		int resultSliderOpponent = (int)opponentSlider.getValue();
		int resultIsActions = actions.isSelected() == true ? 1 : 0;
		int resultIsIterating = iterating.isSelected() == true ? 1 : 0;

		if (resultBeginnerIterations > 0 && resultMediumIterations > 0 && resultHardIterations > 0
				&& resultBeginnerRollOut > 0 && resultMediumRollOut > 0 && resultHardRollOut > 0 && resultPlayer > 0
				&& resultOpponent > 0) {

			int[] nIterations = new int[] { resultBeginnerIterations, resultMediumIterations, resultHardIterations, resultIsIterating };
			int[] nRollOut = new int[] { resultBeginnerRollOut, resultMediumRollOut, resultHardRollOut };
			int[] kActions = new int[] { resultPlayer, resultOpponent , resultIsActions, resultSliderPlayer, resultSliderOpponent};

			Object[] data = {difficultySelected, nIterations, nRollOut, kActions, criteria};
			
			writer.setData(data);
			//writer.setData(difficultySelected, nIterations, nRollOut, kActions);
			writer.execute();

			return true;
		} else {
			return false;
		}
	}

	public static int getSelectionCriteria() {
		return selectionCriteria;
	}

	public static void setSelectionCriteria(Long selectionCriteria) {
		MctsSetup.selectionCriteria = Math.toIntExact(selectionCriteria);
	}
}
