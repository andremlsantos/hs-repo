package net.demilich.metastone.gui.mctsmode;

import java.io.FileReader;
import java.util.Iterator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

//leitura dos parametros de mcts
public class ReadConfigCommand extends Command{

	@Override
	public void setData(Object[] data) { }
	
	@SuppressWarnings("static-access")
	@Override
	public void execute() {
		JSONParser parser = new JSONParser();
		
		try {			
			Object obj = parser.parse(new FileReader(MctsSetup.Path + "\\" + MctsSetup.fileName));			
			JSONObject jsonObject = (JSONObject) obj;			
			Long difficulty = (Long) jsonObject.get("difficulty");		
			Long selectionCriteria = (Long) jsonObject.get("selection-criteria");
			Long[] iterations_array = new Long[4];
			Long[] rollOut_array = new Long[3];
			Long[] actions_array = new Long[5];
			
			// iterations
			JSONArray iterations = (JSONArray) jsonObject.get("iterations");
			Iterator<Long> iterator = iterations.iterator();
			int index = 0;
			
			while (iterator.hasNext()) {				
				iterations_array[index++] = iterator.next().longValue();
			}
			
			//roll-out
			JSONArray rollOut = (JSONArray) jsonObject.get("rollOut");
			iterator = rollOut.iterator();
			index = 0;
			
			while (iterator.hasNext()) {				
				rollOut_array[index++] = iterator.next().longValue();
			}
			
			//actions
			JSONArray actions = (JSONArray) jsonObject.get("actions");
			iterator = actions.iterator();
			index = 0;
			
			while (iterator.hasNext()) {				
				actions_array[index++] = iterator.next().longValue();
			}			
			
			MctsSetup.getInstance().setDifficulty(difficulty);
			MctsSetup.getInstance().setIterations(iterations_array);
			MctsSetup.getInstance().setRollOut(rollOut_array);
			MctsSetup.getInstance().setkActions(actions_array);
			MctsSetup.getInstance().setSelectionCriteria(selectionCriteria);
			
		} catch (Exception e) {
			System.out.println("Error parsing the MCTS Setup: Did you create the MCTS configs");
			
			createFolder(MctsSetup.Path);
		}
	}
}
