package net.demilich.metastone.gui.mctsmode;

import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import net.demilich.metastone.game.behaviour.mcts.WinningSelectionCriteria;

//escrever configuracoes introduzidas no ecra configuracao parametros do mcts
//para json, usamos a biblioteca simple json
public class WriteConfigCommand extends Command{

	private int difficultySelected;
	private int[] nIterations;
	private int[] nRoll_out;
	private int[] kActions;
	private int critiria;
	
	@Override
	public void setData(Object[] data) {
		this.difficultySelected = (int)data[0];
		this.nIterations = (int[])data[1];
		this.nRoll_out = (int[])data[2];
		this.kActions = (int[])data[3];
		this.critiria = (int) data[4];
	}

	@Override
	public void execute() {
		JSONObject json = new JSONObject();
		
		json.put("difficulty", new Integer(this.difficultySelected));
		
		JSONArray iterations = new JSONArray();
		for(int i=0; i<nIterations.length; i++) {
			iterations.add(nIterations[i]);
		}
		json.put("iterations", iterations);
		
		JSONArray rollOut = new JSONArray();
		for(int i=0; i<nRoll_out.length; i++) {
			rollOut.add(nRoll_out[i]);
		}
		json.put("rollOut", rollOut);
		
		JSONArray actions = new JSONArray();
		for(int i=0; i<kActions.length; i++) {
			actions.add(kActions[i]);
		}

		json.put("actions", actions);
		
		json.put("selection-criteria", critiria);
		
		try {
			writeToFile(MctsSetup.Path, MctsSetup.fileName, json);
			//writeJson(MctsSetup.fileName, json);
		} catch (IOException e) {
			System.out.println("Error saving MCTS configs");
		}
	}
}
