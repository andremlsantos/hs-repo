package net.demilich.metastone.gui.mctsmode;

import java.util.ArrayList;
import java.util.List;

import javafx.application.Platform;
import net.demilich.metastone.GameNotification;
import net.demilich.metastone.game.decks.Deck;
import net.demilich.metastone.gui.simulationmode.WaitForSimulationView;
import net.demilich.metastone.utils.Tuple;
import net.demilich.nittygrittymvc.Mediator;
import net.demilich.nittygrittymvc.interfaces.INotification;

//padrao desenho do simualdor
//para ligar UI com algoritmo genetico
public class GeneticTradeMediator extends Mediator<GameNotification> {

	public static final String NAME = "GENETICMediator";

	// VIEW
	private final GeneticTradeView view;
	private final WaitForSimulationView waitView;

	public GeneticTradeMediator() {
		super(NAME);
		view = new GeneticTradeView();
		waitView = new WaitForSimulationView();
	}

	@SuppressWarnings("unused")
	@Override
	public void handleNotification(INotification<GameNotification> notification) {
		switch (notification.getId()) {
		case SIMULATION_PROGRESS_UPDATE:
			Tuple<Integer, Integer> progress = (Tuple<Integer, Integer>) notification.getBody();
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					waitView.update(progress.getFirst(), progress.getSecond());
				}
			});
			break;
		case COMMIT_SIMULATIONMODE_CONFIG:			
			getFacade().sendNotification(GameNotification.SIMULATE_GENETIC, notification.getBody());
			getFacade().sendNotification(GameNotification.SHOW_MODAL_DIALOG, waitView);
			break;
		case HIDE_SIMULATION_PROGRESS_UPDATE:
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					waitView.getScene().getWindow().hide();
				}
			});
			break;
		case PRINT_GENETIC_WEIGHTS:
			view.print(notification.getBody());
			break;
		case SAVE_GENETIC_WEIGHTS:
			view.save(notification.getBody());
			break;
		case REPLY_DECKS:
			getFacade().sendNotification(GameNotification.RECEIVE_DECKS, notification.getBody());
		break;
		default:
			break;
		}
	}
	
	@Override
	public List<GameNotification> listNotificationInterests() {
		List<GameNotification> notificationInterests = new ArrayList<GameNotification>();
		notificationInterests.add(GameNotification.COMMIT_SIMULATIONMODE_CONFIG);
		notificationInterests.add(GameNotification.SIMULATION_PROGRESS_UPDATE);
		notificationInterests.add(GameNotification.HIDE_SIMULATION_PROGRESS_UPDATE);
		notificationInterests.add(GameNotification.PRINT_GENETIC_WEIGHTS);
		notificationInterests.add(GameNotification.SAVE_GENETIC_WEIGHTS);
		notificationInterests.add(GameNotification.REPLY_DECKS);
		return notificationInterests;
	}
	
	@Override
	public void onRegister() {		
		//mostrar a view
		getFacade().sendNotification(GameNotification.SHOW_VIEW, view);
		getFacade().sendNotification(GameNotification.REQUEST_DECKS);
	}
}
