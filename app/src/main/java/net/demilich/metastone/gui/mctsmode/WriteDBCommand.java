package net.demilich.metastone.gui.mctsmode;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javafx.scene.control.CheckBox;
import net.demilich.metastone.game.decks.Deck;
import net.demilich.metastone.game.entities.heroes.HeroClass;

//escrever baralhos BD selecionados para json
//para json, usamos a biblioteca simple json
public class WriteDBCommand extends Command{

	//view
	private ArrayList<HeroBox> view;
		
	public ArrayList<HeroBox> getView() {
		return view;
	}
	
	@Override
	public void setData(Object[] data) {
		ArrayList<HeroBox> heroBox = (ArrayList<HeroBox>)data[0];
		this.view = heroBox;
	}
	
	@Override
	public void execute() {
		JSONObject json = new JSONObject();

		ArrayList<HeroClass> heroes = HeroClass.ANY.getHeroes();
		
		for(int i=0; i<heroes.size(); i++) {
			buildJson(heroes.get(i), json, view.get(i).getDecks());
		}

		try {
			writeToFile(DecksDB.Path, DecksDB.fileName, json);
			//writeJson(DecksDB.fileName, json);
		} catch (IOException e) {
			System.out.println("Error saving DB DECKS");
		}
		
	}
	
	private static void buildJson(HeroClass hero, JSONObject json, ArrayList<CheckBox> decks) {
		// json array
		JSONArray list = new JSONArray();

		for (CheckBox box : decks) {
			if(box.isSelected())
				list.add(box.getText());
		}

		json.put(hero.name(), list);
	}
}
