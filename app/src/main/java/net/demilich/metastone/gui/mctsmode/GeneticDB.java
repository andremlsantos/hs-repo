package net.demilich.metastone.gui.mctsmode;

import java.io.File;
import java.util.ArrayList;
import net.demilich.metastone.utils.UserHomeMetastone;

/*
 * leitura e escrita dos pesos da heuristica em json
 * */
public class GeneticDB {

	// singleton
	private static GeneticDB instance = null;

	// VARIABLES
	public static final String DB_FOLDER = "geneticTraining";
	public static final String Path = UserHomeMetastone.getPath() + File.separator + DB_FOLDER;

	// read & write
	private Command reader = new ReadGeneticDBCommand();
	private Command writer = new WriteGeneticDBCommand();
	
	private double[] values;

	public static GeneticDB getInstance() {
		if (instance == null) {
			instance = new GeneticDB();
		}
		return instance;
	}
	
	public void writeGeneticDeck(Object[] configs) {
		writer.setData(configs);
		try {
			writer.execute();
		} catch (Exception e) {
			System.out.println("Error writing genetic deck, 37, GeneticDB");
		}
	}
	
	public void readGeneticDeck(String deckName) {
		Object[] data = {deckName};
		reader.setData(data);
		try {
			reader.execute();
		} catch (Exception e) {
			System.out.println("Error reading genetic deck, 47, GeneticDB");
		}
	}
	
	/*
	 * GENETIC DECKS
	 * 
	 * AMNESIAC�S SECRET FACE; 
	 * CHE0NSU�S YOGG TEMPO MAGE;
	 * FR0ZEN�S N�ZOTH PALADIN;
	 * JASONZHOU�S N�ZOTH CONTROL WARRIOR;
	 * TAREI�S WARLOCK ZOO;
	 * YULSIC�S MIDRANGE SHAMAN;
	 */	
	public ArrayList<String> getGeneticDecksNames() {
		ArrayList<String> geneticDeckList = new ArrayList<>();
		
		geneticDeckList.add("AMNESIAC�S SECRET FACE");
		geneticDeckList.add("JASONZHOU�S N�ZOTH CONTROL WARRIOR");
		geneticDeckList.add("FR0ZEN�S N�ZOTH PALADIN");
		geneticDeckList.add("YULSIC�S MIDRANGE SHAMAN");
		geneticDeckList.add("CHE0NSU�S YOGG TEMPO MAGE");
		geneticDeckList.add("TAREI�S WARLOCK ZOO");
		
		return geneticDeckList;
	}

	public double[] getValues() {
		return values;
	}

	public void setValues(double[] values) {
		this.values = values;
	}
}
